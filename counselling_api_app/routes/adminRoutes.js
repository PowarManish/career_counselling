const express = require("express");
const loginActions = require("../actions/Admin/loginActions");
const candidateActions = require("../actions/Admin/candidateActions");

router = express.Router();

router.get("/login", (req, res) => {
  loginActions.authenticateLogin(req, res);
});

router.post("/counselling", (req, res) => {
  candidateActions.postCounsellingConclusion(req, res);
});

router.post("/mailConsellingResult", (req, res) => {
  candidateActions.postCounsellingMail(req, res);
});

module.exports = router;
