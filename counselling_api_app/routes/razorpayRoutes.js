const express = require("express");
const razorpayActions = require("../actions/razorpayActions");

router = express.Router();

router.post("/createOrder", (req, res) => {
  razorpayActions.createOrder(req, res);
});
//Below route is for webhook :TO be Implemented
// router.post("/verification", (req, res) => {
//   razorpayActions.verification(req, res);
// });

router.post("/success", (req, res) => {
  razorpayActions.success(req, res);
});
module.exports = router;
