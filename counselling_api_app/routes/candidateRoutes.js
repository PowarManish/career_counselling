const express = require("express");
const candidateActions = require("../actions/candidateActions");

router = express.Router();

router.post("/info", (req, res) => {
  candidateActions.postCandidateInfo(req, res);
});

router.get("/table_data", (req, res) => {
  candidateActions.getCandidateInfo(req, res);
});

router.get("/resume_test", (req, res) => {
  candidateActions.resumeTest(req, res);
});

router.post("/mailResult", (req, res) => {
  candidateActions.mailResult(req, res);
});

router.post("/savePageNumber", (req, res) => {
  candidateActions.changePageToResume(req, res);
});
module.exports = router;
