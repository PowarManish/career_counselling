const express = require("express");
const testActions = require("../actions/testActions");

router = express.Router();

router.post("/save_answer", (req, res) => {
  testActions.saveAnswer(req, res);
});

router.put("/save_answer", (req, res) => {
  testActions.updateAnswer(req, res);
});

router.get("/", (req, res) => {
  testActions.getTestData(req, res);
});

router.post("/submit", (req, res) => {
  testActions.submitTest(req, res);
});

router.get("/result", (req, res) => {
  testActions.getResult(req, res);
});
router.get("/adjustmentAns", (req, res) => {
  testActions.getAdjustmentAnswer(req, res);
});

module.exports = router;
