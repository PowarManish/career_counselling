var mysql = require("mysql");

var pool = mysql.createPool({
  connectionLimit: 100,
  waitForConnections: true,
  queueLimit: 0,
  host: "localhost",
  user: "root",
  password: "Pushkar1@", //Password for VPS DATABASE
  database: "careercounselling",
  debug: true,
  wait_timeout: 28800,
  connect_timeout: 10,
});

module.exports = pool;
