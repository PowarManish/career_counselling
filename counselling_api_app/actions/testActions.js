const pool = require("../dbConnection");
const utility = require("../utility");

saveAnswer = (req, res) => {
  let equ = {
    candidate_id: req.body.data.candidateId,
    test_id: req.body.data.testId,
    question_number: req.body.data.questionNumber,
    selected_answer: req.body.data.answer,
    correct_answer: req.body.data.correctAnswer,
    remaining_time: req.body.data.remainingSeconds,
    timestamp: utility.getCurrentDateTime(),
  };
  const INSERT_INTO_ANSWER = "INSERT INTO answer SET ?";
  pool.query(INSERT_INTO_ANSWER, equ, (err, results) => {
    if (err) {
      console.log(err);
      return res.send(err);
    } else {
      return res.status(200).json({ isDataChanged: true });
    }
  });
};

updateAnswer = (req, res) => {
  const UPDATE_ANSWER =
    "UPDATE answer set selected_answer = ? , remaining_time = ? ,timestamp = ? WHERE candidate_id = ? AND test_id = ? AND question_number = ?";
  pool.query(
    UPDATE_ANSWER,
    [
      req.body.data.answer,
      req.body.data.remainingSeconds,
      utility.getCurrentDateTime(),
      req.body.data.candidateId,
      req.body.data.testId,
      req.body.data.questionNumber,
    ],
    (err, results) => {
      if (err) {
        console.log(err);
        return res.send(err);
      } else {
        return res.status(200).json({ isDataChanged: true });
      }
    }
  );
};

getTestData = (req, res) => {
  const SELECT_TEST_DATA = "Select * from test";
  pool.query(SELECT_TEST_DATA, (err, results) => {
    if (err) {
      return res.send(err);
    } else {
      return res.json({
        data: results,
      });
    }
  });
};

getResult = (req, res) => {
  const SELECT_RESULT = "Select * from result where candidate_id = ?";
  pool.query(SELECT_RESULT, [req.query.candidate_id], (err, results) => {
    if (err) {
      return res.send(err);
    } else {
      return res.json({
        data: results,
      });
    }
  });
};
submitTest = (req, res) => {
  var correct_ans_count = 0;
  var wrong_ans_count = 0;
  var right_ans_id = [];
  if (
    req.body.data.testName == "Analogy" &&
    req.body.data.testName == "Series"
  ) {
    return res.status(200).json({ isDataChanged: true });
  } else if (req.body.data.testName == "Interest") {
    const SELECT_RIGHT_ANSWER_QUESTION_NUMBERS =
      "Select question_number from answer where selected_answer = correct_answer AND candidate_id = ? AND test_id =?";
    pool.query(
      SELECT_RIGHT_ANSWER_QUESTION_NUMBERS,
      [req.body.data.candidateId, req.body.data.testId],
      (err, results) => {
        if (err) {
          return res.send(err);
        } else {
          results.forEach((element) => {
            right_ans_id.push(element.question_number);
          });

          var interestInventoryscores = getInterestInventoryScore(right_ans_id);

          var medStanine = getStanine(
            [2, 6, 11, 16, 20, 24, 27, 29, 30],
            interestInventoryscores[0]
          );
          var enggStanine = getStanine(
            [1, 4, 9, 14, 19, 24, 27, 29, 30],
            interestInventoryscores[1]
          );
          var commStanine = getStanine(
            [0, 3, 7, 12, 17, 21, 25, 28, 30],
            interestInventoryscores[2]
          );
          var artStanine = getStanine(
            [1, 4, 8, 12, 17, 22, 26, 29, 30],
            interestInventoryscores[3]
          );
          var fartStanine = getStanine(
            [0, 3, 6, 11, 17, 21, 25, 28, 30],
            interestInventoryscores[4]
          );
          let insertData = [
            [
              req.body.data.candidateId,
              "Medical",
              interestInventoryscores[0],
              medStanine,
            ],
            [
              req.body.data.candidateId,
              "Engineering",
              interestInventoryscores[1],
              enggStanine,
            ],
            [
              req.body.data.candidateId,
              "Commerce",
              interestInventoryscores[2],
              commStanine,
            ],
            [
              req.body.data.candidateId,
              "Arts",
              interestInventoryscores[3],
              artStanine,
            ],
            [
              req.body.data.candidateId,
              "Fine Arts",
              interestInventoryscores[4],
              fartStanine,
            ],
          ];
          let INSERT_INTO_RESULT =
            "INSERT INTO result (`candidate_id`, `test_name`, `score`,`stanine`) VALUES ?";
          pool.query(INSERT_INTO_RESULT, [insertData], (err, results) => {
            if (err) {
              console.log(err);
              return res.send(err);
            } else {
              return res.status(200).json({ isDataChanged: true });
            }
          });
        }
      }
    );
  } else if (req.body.data.testName == "Adjustment") {
    const SELECT_RIGHT_ANSWER_QUESTION_NUMBERS =
      "Select question_number from answer where selected_answer = correct_answer AND candidate_id = ? AND test_id =?";
    pool.query(
      SELECT_RIGHT_ANSWER_QUESTION_NUMBERS,
      [req.body.data.candidateId, req.body.data.testId],
      (err, results) => {
        if (err) {
          return res.send(err);
        } else {
          results.forEach((element) => {
            right_ans_id.push(element.question_number);
          });
          var Adjustmentscores = getAdjustmentScore(right_ans_id);

          var homeRemark = getAdjustmentRemark(
            [2, 4, 10, 16, 17],
            Adjustmentscores[0]
          );
          var HealthRemark = getAdjustmentRemark(
            [1, 5, 12, 18, 19],
            Adjustmentscores[1]
          );
          var SocialRemark = getSocialRemark(Adjustmentscores[2]);
          var EmotionRemark = getAdjustmentRemark(
            [6, 10, 18, 25, 26],
            Adjustmentscores[3]
          );
          let insertData = [
            [
              req.body.data.candidateId,
              "Home",
              Adjustmentscores[0],
              homeRemark,
            ],
            [
              req.body.data.candidateId,
              "Health",
              Adjustmentscores[1],
              HealthRemark,
            ],
            [
              req.body.data.candidateId,
              "Social",
              Adjustmentscores[2],
              SocialRemark,
            ],
            [
              req.body.data.candidateId,
              "Emotion",
              Adjustmentscores[3],
              EmotionRemark,
            ],
          ];
          let INSERT_INTO_RESULT =
            "INSERT INTO result (`candidate_id`, `test_name`, `score`,`stanine`) VALUES ?";
          pool.query(INSERT_INTO_RESULT, [insertData], (err, results) => {
            if (err) {
              console.log(err);
              return res.send(err);
            } else {
              return res.status(200).json({ isDataChanged: true });
            }
          });
        }
      }
    );
  } else {
    var score = 0;
    if (req.body.data.testName == "Classification") {
      const SELECT_RIGHT_ANSWER_NVTI_COUNT =
        "Select count(id) AS `CorrectAnswerCount` from answer where selected_answer = correct_answer AND candidate_id = ? AND test_id IN (12, 13, 14,15)";
      pool.query(
        SELECT_RIGHT_ANSWER_NVTI_COUNT,
        [req.body.data.candidateId],
        (err, results) => {
          if (err) {
            return res.send(err);
          } else {
            correct_ans_count = results[0].CorrectAnswerCount;
            const SELECT_WRONG_ANSWER_NVTI_COUNT =
              "Select count(id) AS `WrongAnswerCount` from answer where selected_answer != correct_answer AND candidate_id = ? AND test_id IN (12, 13, 14,15)";
            pool.query(
              SELECT_WRONG_ANSWER_NVTI_COUNT,
              [req.body.data.candidateId],
              (err, results) => {
                if (err) {
                  return res.send(err);
                } else {
                  wrong_ans_count = results[0].WrongAnswerCount;
                  score = correct_ans_count - 0.25 * wrong_ans_count;

                  let equ = {
                    candidate_id: req.body.data.candidateId,
                    test_name: "NVTI",
                    score: score,
                    stanine: calculateStanine(score, req.body.data.testName),
                  };
                  let INSERT_INTO_RESULT = "INSERT INTO result SET ?";
                  pool.query(INSERT_INTO_RESULT, equ, (err, results) => {
                    if (err) {
                      console.log(err);
                      return res.send(err);
                    } else {
                      return res.status(200).json({ isDataChanged: true });
                    }
                  });
                }
              }
            );
          }
        }
      );
    } else if (req.body.data.testName == "Reflection") {
      const SELECT_RIGHT_ANSWER_REFLECTION_COUNT =
        "Select count(id) AS `CorrectAnswerCount` from answer where selected_answer = correct_answer AND candidate_id = ? AND test_id = ?";
      pool.query(
        SELECT_RIGHT_ANSWER_REFLECTION_COUNT,
        [req.body.data.candidateId, req.body.data.testId],
        (err, results) => {
          if (err) {
            return res.send(err);
          } else {
            correct_ans_count = results[0].CorrectAnswerCount;
            const SELECT_WRONG_ANSWER_REFLECTION_COUNT =
              "Select count(id) AS `WrongAnswerCount` from answer where selected_answer != correct_answer AND candidate_id = ? AND test_id = ?";
            pool.query(
              SELECT_WRONG_ANSWER_REFLECTION_COUNT,
              [req.body.data.candidateId, req.body.data.testId],
              (err, results) => {
                if (err) {
                  return res.send(err);
                } else {
                  wrong_ans_count = results[0].WrongAnswerCount;
                  score = correct_ans_count - 0.25 * wrong_ans_count;

                  let equ = {
                    candidate_id: req.body.data.candidateId,
                    test_name: "Reflection",
                    score: score,
                  };
                  let INSERT_INTO_RESULT = "INSERT INTO result SET ?";
                  pool.query(INSERT_INTO_RESULT, equ, (err, results) => {
                    if (err) {
                      console.log(err);
                      return res.send(err);
                    } else {
                      return res.status(200).json({ isDataChanged: true });
                    }
                  });
                }
              }
            );
          }
        }
      );
    } else {
      const SELECT_RIGHT_ANSWER_COUNT =
        "Select count(id) AS `CorrectAnswerCount` from answer where selected_answer = correct_answer AND candidate_id = ? AND test_id = ?";
      pool.query(
        SELECT_RIGHT_ANSWER_COUNT,
        [req.body.data.candidateId, req.body.data.testId],
        (err, results) => {
          if (err) {
            return res.send(err);
          } else {
            correct_ans_count = results[0].CorrectAnswerCount;
            score = correct_ans_count;
            let stanine = calculateStanine(score, req.body.data.testName);
            let remark = getRemark(stanine);
            let equ = {
              candidate_id: req.body.data.candidateId,
              test_name: req.body.data.testName,
              score: score,
              stanine: stanine,
              remark: remark,
            };
            let INSERT_INTO_RESULT = "INSERT INTO result SET ?";
            pool.query(INSERT_INTO_RESULT, equ, (err, results) => {
              if (err) {
                console.log(err);
                return res.send(err);
              } else {
                return res.status(200).json({ isDataChanged: true });
              }
            });
          }
        }
      );
    }
  }
};

calculateStanine = (score, test_name) => {
  let stanine = 0;

  switch (test_name) {
    case "NC":
      stanine = getStanine([16, 22, 27, 32, 38, 42, 45, 48, 78], score);
      break;
    case "AC":
      stanine = getStanine([5, 7, 11, 13, 18, 21, 24, 28, 29], score);
      break;
    case "TDS":
      stanine = getStanine([4, 5, 7, 9, 11, 14, 16, 21, 22], score);
      break;
    case "VE":
      stanine = getStanine([0, 1, 2, 5, 7, 8, 10, 14, 16], score);
      break;
    case "TM":
      stanine = getStanine([8, 10, 15, 18, 21, 24, 27, 31, 34], score);
      break;
    case "AR":
      stanine = getStanine([1, 2, 5, 6, 8, 9, 11, 14, 18], score);
      break;
    case "FM":
      stanine = getStanine([6, 10, 12, 16, 20, 22, 25, 28, 29], score);
      break;
    case "NVR":
      stanine = getStanine([1, 5, 7, 9, 12, 14, 17, 20, 22], score);
      break;
    case "VM":
      stanine = getStanine([5, 6, 9, 12, 14, 18, 21, 25, 29], score);
      break;
    case "VH":
      stanine = getStanine([5, 6, 9, 12, 14, 18, 21, 25, 29], score);
      break;
    case "Classification":
      stanine = getStanine([11, 19, 27, 35, 43, 54, 63, 68, 80], score);
      break;
    default:
      console.log("Default");
  }
  return stanine;
};

getStanine = (arr, score) => {
  if (score <= arr[0]) {
    return 1;
  } else if (score > arr[0] && score <= arr[1]) {
    return 2;
  } else if (score > arr[1] && score <= arr[2]) {
    return 3;
  } else if (score > arr[2] && score <= arr[3]) {
    return 4;
  } else if (score > arr[3] && score <= arr[4]) {
    return 5;
  } else if (score > arr[4] && score <= arr[5]) {
    return 6;
  } else if (score > arr[5] && score <= arr[6]) {
    return 7;
  } else if (score > arr[6] && score <= arr[7]) {
    return 8;
  } else if (score > arr[7]) {
    return 9;
  }
};

getInterestInventoryScore = (arr) => {
  var medi = [],
    engg = [],
    comm = [],
    arts = [],
    farts = [];
  for (let i = 0; i < arr.length; i++) {
    if (
      [
        1,
        6,
        11,
        16,
        21,
        26,
        31,
        36,
        41,
        46,
        51,
        56,
        61,
        66,
        71,
        76,
        81,
        86,
        91,
        96,
        101,
        106,
        111,
        116,
        121,
        126,
        131,
        136,
        141,
        146,
      ].includes(arr[i])
    ) {
      medi.push(arr[i]);
    } else if (
      [
        2,
        7,
        12,
        17,
        22,
        27,
        32,
        37,
        42,
        47,
        52,
        57,
        62,
        67,
        72,
        77,
        82,
        87,
        92,
        97,
        102,
        107,
        112,
        117,
        122,
        127,
        132,
        137,
        142,
        147,
      ].includes(arr[i])
    ) {
      engg.push(arr[i]);
    } else if (
      [
        3,
        8,
        13,
        18,
        23,
        28,
        33,
        38,
        43,
        48,
        53,
        58,
        63,
        68,
        73,
        78,
        83,
        88,
        93,
        98,
        103,
        108,
        113,
        118,
        123,
        128,
        133,
        138,
        143,
        148,
      ].includes(arr[i])
    ) {
      comm.push(arr[i]);
    } else if (
      [
        4,
        9,
        14,
        19,
        24,
        29,
        34,
        39,
        44,
        49,
        54,
        59,
        64,
        69,
        74,
        79,
        84,
        89,
        94,
        99,
        104,
        109,
        114,
        119,
        124,
        129,
        134,
        139,
        144,
        149,
      ].includes(arr[i])
    ) {
      arts.push(arr[i]);
    } else if (
      [
        5,
        10,
        15,
        20,
        25,
        30,
        35,
        40,
        45,
        50,
        55,
        60,
        65,
        70,
        75,
        80,
        85,
        90,
        95,
        100,
        105,
        110,
        115,
        120,
        125,
        130,
        135,
        140,
        145,
        150,
      ].includes(arr[i])
    ) {
      farts.push(arr[i]);
    }
  }
  return [medi.length, engg.length, comm.length, arts.length, farts.length];
};

getAdjustmentScore = (arr) => {
  var home = [],
    health = [],
    social = [],
    emotion = [];
  for (let i = 0; i < arr.length; i++) {
    if (
      [
        1,
        5,
        9,
        13,
        17,
        21,
        25,
        29,
        33,
        37,
        41,
        45,
        49,
        53,
        57,
        61,
        65,
        69,
        73,
        77,
        81,
        85,
        89,
        93,
        97,
        101,
        105,
        109,
        113,
        117,
        121,
        125,
        129,
        133,
        137,
      ].includes(arr[i])
    ) {
      home.push(arr[i]);
    } else if (
      [
        2,
        6,
        10,
        14,
        18,
        22,
        26,
        30,
        34,
        38,
        42,
        46,
        50,
        54,
        58,
        62,
        66,
        70,
        74,
        78,
        82,
        86,
        90,
        94,
        98,
        102,
        106,
        110,
        114,
        118,
        122,
        126,
        130,
        134,
        138,
      ].includes(arr[i])
    ) {
      health.push(arr[i]);
    } else if (
      [
        3,
        7,
        11,
        15,
        19,
        23,
        27,
        31,
        35,
        39,
        43,
        47,
        51,
        55,
        59,
        63,
        67,
        71,
        75,
        79,
        83,
        87,
        91,
        95,
        99,
        103,
        107,
        111,
        115,
        119,
        123,
        127,
        131,
        135,
        139,
      ].includes(arr[i])
    ) {
      social.push(arr[i]);
    } else if (
      [
        4,
        8,
        12,
        16,
        20,
        24,
        28,
        32,
        36,
        40,
        44,
        48,
        52,
        56,
        60,
        64,
        68,
        72,
        76,
        80,
        84,
        88,
        92,
        96,
        100,
        104,
        108,
        112,
        116,
        120,
        124,
        128,
        132,
        136,
        140,
      ].includes(arr[i])
    ) {
      emotion.push(arr[i]);
    }
  }
  return [home.length, health.length, social.length, emotion.length];
};

getAdjustmentRemark = (arr, score) => {
  if (score <= arr[0]) {
    return "Excellent";
  } else if (score > arr[0] && score <= arr[1]) {
    return "Good";
  } else if (score > arr[1] && score <= arr[2]) {
    return "Average";
  } else if (score > arr[2] && score <= arr[3]) {
    return "Unsatisfied";
  } else if (score > arr[3]) {
    return "Very Unsatisfied";
  }
};

getSocialRemark = (score) => {
  if (score <= 5) {
    return "V.Agg";
  } else if (score > 5 && score <= 9) {
    return "Agg";
  } else if (score > 9 && score <= 18) {
    return "Ave";
  } else if (score > 18 && score <= 25) {
    return "Ret";
  } else if (score > 25) {
    return "V.Ret";
  }
};

getRemark = (stanine) => {
  if (stanine < 4) {
    return "Weak";
  } else if (stanine < 6) {
    return "Average";
  } else if (stanine < 8) {
    return "Good";
  } else return "Best";
};
getAdjustmentAnswer = (req, res) => {
  const SELECT_ADJUSTMENT_ANSWER =
    "SELECT question_number,selected_answer FROM careercounselling.answer where test_id = ? && candidate_id = ?";
  pool.query(
    SELECT_ADJUSTMENT_ANSWER,
    [17, req.query.candidate_id],
    (err, results) => {
      if (err) {
        return res.send(err);
      } else {
        return res.json({
          data: results,
        });
      }
    }
  );
};
module.exports.saveAnswer = saveAnswer;
module.exports.updateAnswer = updateAnswer;
module.exports.getTestData = getTestData;
module.exports.submitTest = submitTest;
module.exports.getResult = getResult;
module.exports.getAdjustmentAnswer = getAdjustmentAnswer;
