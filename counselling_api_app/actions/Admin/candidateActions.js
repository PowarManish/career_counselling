const pool = require("../../dbConnection");
const mailActions = require("../mailActions");

postCounsellingConclusion = (req, res) => {
  let equ = {
    Conclusion: req.body.data.values.conclusion,
    isCounsellingDone: true,
  };

  let UPDATE_CANDIDATE = "UPDATE candidate SET ? WHERE id = ?";
  pool.query(
    UPDATE_CANDIDATE,
    [equ, req.body.data.candidateId],
    (err, results) => {
      if (err) {
        console.log(err);
        return res.send(err);
      } else {
        return res.json({
          isDataUpdated: true,
        });
      }
    }
  );
};
postCounsellingMail = (req, res) => {
  let message =
    "<p>Hi,</p>" +
    "<p>As per discussion in counselling we have send you conclusion.</p>" +
    "<p>Diwakar Thanekar - +91 9890972463</p>" +
    "<p>RESULT</p>" +
    "<h3>GATB</h3>" +
    '<table style="border: 1px solid #333;">' +
    "<thead>" +
    "<th> Test Name </th>" +
    "<th> Score </th>" +
    "<th> Stanine </th>" +
    "<th> Remark </th>" +
    "</thead>";

  for (let i = 0; i < req.body.data.gatbDataSource.length; i++) {
    message +=
      "<tr>" +
      "<td>" +
      req.body.data.gatbDataSource[i].test_name +
      "</td>" +
      "<td>" +
      req.body.data.gatbDataSource[i].score +
      "</td>" +
      "<td>" +
      req.body.data.gatbDataSource[i].stanine +
      "</td>" +
      "<td>" +
      req.body.data.gatbDataSource[i].remark +
      "</td>" +
      "</tr>";
  }

  message +=
    "</table>" +
    "<br></br>" +
    "<h3>NVTI</h3>" +
    '<table style="border: 1px solid #333;">' +
    "<thead>" +
    "<th> Test Name </th>" +
    "<th> Score </th>" +
    "<th> Stanine </th>";
  ("</thead>");

  for (let i = 0; i < req.body.data.nvtiDataSource.length; i++) {
    message +=
      "<tr>" +
      "<td>" +
      req.body.data.nvtiDataSource[i].test_name +
      "</td>" +
      "<td>" +
      req.body.data.nvtiDataSource[i].score +
      "</td>" +
      "<td>" +
      req.body.data.nvtiDataSource[i].stanine +
      "</td>" +
      "</tr>";
  }
  message +=
    "</table>" +
    "<br></br>" +
    "<h3>Interest</h3>" +
    '<table style="border: 1px solid #333;">' +
    "<thead>" +
    "<th> Test Name </th>" +
    "<th> Score </th>" +
    "<th> Stanine </th>";
  ("</thead>");

  for (let i = 0; i < req.body.data.interestDataSource.length; i++) {
    message +=
      "<tr>" +
      "<td>" +
      req.body.data.interestDataSource[i].test_name +
      "</td>" +
      "<td>" +
      req.body.data.interestDataSource[i].score +
      "</td>" +
      "<td>" +
      req.body.data.interestDataSource[i].stanine +
      "</td>" +
      "</tr>";
  }

  message +=
    "</table>" +
    "<br></br>" +
    "<h3>Adjustment</h3>" +
    '<table style="border: 1px solid #333;">' +
    "<thead>" +
    "<th> Test Name </th>" +
    "<th> Score </th>" +
    "<th> Remark </th>";
  ("</thead>");

  for (let i = 0; i < req.body.data.adjustmentDataSource.length; i++) {
    message +=
      "<tr>" +
      "<td>" +
      req.body.data.adjustmentDataSource[i].test_name +
      "</td>" +
      "<td>" +
      req.body.data.adjustmentDataSource[i].score +
      "</td>" +
      "<td>" +
      req.body.data.adjustmentDataSource[i].stanine +
      "</td>" +
      "</tr>";
  }

  message += "</table>";
  message += "<h3>Conclusion :-</h3>";
  message += req.body.data.conclusion;
  message += "<p>All the best for your future</p>";

  message += "<h2>Thank you!!!</h2>";
  mailActions.sendMail(
    "Result with Conclusion",
    message,
    req.body.data.email,
    res
  );
};
module.exports.postCounsellingConclusion = postCounsellingConclusion;
module.exports.postCounsellingMail = postCounsellingMail;
