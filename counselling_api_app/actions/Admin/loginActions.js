const pool = require("../../dbConnection");
authenticateLogin = (req, res) => {
  const GET_AUTH =
    "Select * from login_info where username = ? AND password = ?";
  pool.query(
    GET_AUTH,
    [req.query.username, req.query.password],
    (err, results) => {
      if (err) {
        return res.send(err);
      } else {
        if (results.length > 0) {
          return res.json({
            data: true,
          });
        } else {
          return res.json({
            data: false,
          });
        }
      }
    }
  );
};

module.exports.authenticateLogin = authenticateLogin;
