const pool = require("../dbConnection");
const Razorpay = require("razorpay");
const shortid = require("shortid");

createOrder = async (req, res) => {
  const payment_capture = 1;
  const amount = 1000;
  const currency = "INR";
  const options = {
    amount: amount * 100,
    currency,
    receipt: shortid.generate(),
    payment_capture,
  };
  const razorpay = new Razorpay({
    key_id: "rzp_live_agbLuznJRx4noY",
    key_secret: "Y6UNLuqEGIq7qtoQxc4adrrr",
  });
  try {
    const response = await razorpay.orders.create(options);
    const INSERT_ORDER_ID_TO_CANDIDATE =
      "UPDATE candidate set razorpay_order_id = ? WHERE id = ?";
    pool.query(
      INSERT_ORDER_ID_TO_CANDIDATE,
      [response.id, req.body.candidate_id],
      (err, results) => {
        if (err) {
          console.log(err);
          return res.send(err);
        } else {
          // return res.status(200).json({ isDataChanged: true });
          res.json({
            id: response.id,
            currency: response.currency,
            amount: response.amount,
          });
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
};
//TO be Implemented (WEBHOOK)
// verification = (req, res) => {
//   const secret = "RazorpayVerification";

//   const crypto = require("crypto");

//   const shasum = crypto.createHmac("sha256", secret);
//   shasum.update(JSON.stringify(req.body));
//   const digest = shasum.digest("hex");

//   console.log(digest, req.headers["x-razorpay-signature"]);

//   if (digest === req.headers["x-razorpay-signature"]) {
//     console.log("request is legit");
//   } else {
//     // pass it
//   }
//   res.json({ status: "ok" });
// };
success = (req, res) => {
  const secret = "Y6UNLuqEGIq7qtoQxc4adrrr";
  const crypto = require("crypto");
  const shasum = crypto.createHmac("sha256", secret);
  shasum.update(`${req.body.order_id}|${req.body.razorpay_payment_id}`);
  const digest = shasum.digest("hex");

  if (digest == req.body.razorpay_signature) {
    const INSERT_PAYMENT_ID_TO_CANDIDATE =
      "UPDATE candidate set razorpay_payment_id = ?,onPage = 1 WHERE id = ?";
    pool.query(
      INSERT_PAYMENT_ID_TO_CANDIDATE,
      [req.body.razorpay_payment_id, req.body.candidate_id],
      (err, results) => {
        if (err) {
          console.log(err);
          return res.send(err);
        } else {
          // return res.status(200).json({ isDataChanged: true });
          res.json({
            isLegit: true,
          });
        }
      }
    );
  }
};
module.exports.createOrder = createOrder;
module.exports.success = success;
// module.exports.verification = verification;
