const pool = require("../dbConnection");
const utility = require("../utility");
const mailActions = require("./mailActions");

postCandidateInfo = (req, res) => {
  let equ = {
    name: req.body.data.name,
    address: req.body.data.permanant_address,
    phone_number: req.body.data.phone_number,
    email: req.body.data.email,
    school_name: req.body.data.school_name,
    standard: req.body.data.standard,
    date_of_birth: req.body.data.dob,
    age: req.body.data.age,
    mother_tongue: req.body.data.motherTongue,
    medium_of_instruction: req.body.data.mediumOfInstruction,
    hobby: req.body.data.hobby,
    fav_book_type: req.body.data.typeOfBook,
    is_separate_room_available: req.body.data.studyRoom,
    is_private_tution: req.body.data.privateTution,
    private_tution_subjects: req.body.data.privateTutionSubjects,
    preferred_streams: JSON.stringify(req.body.data.preferredStreams),
    favourite_subject: req.body.data.favouriteSubject,
    disliked_subject: req.body.data.dislikedSubject,
    parents_preferred_stream: req.body.data.parentsChoice,
    candidate_preferred_stream: req.body.data.candidatesChoice,
    isCounsellingDone: false,
    created_on: utility.getCurrentDateTime(),
    onPage: 0,
  };

  const INSERT_INTO_CANDIDATE = "INSERT INTO candidate SET ?";
  pool.query(INSERT_INTO_CANDIDATE, equ, (err, results) => {
    if (err) {
      console.log(err);
      return res.send(err);
    } else {
      return res.status(200).json({ candidateId: results.insertId });
    }
  });
};

getCandidateInfo = (req, res) => {
  const SELECT_CANDIDATE_INFO =
    "Select * from candidate ORDER BY created_on DESC ";

  pool.query(SELECT_CANDIDATE_INFO, (err, results) => {
    if (err) {
      return res.send(err);
    } else {
      return res.json({
        data: results,
      });
    }
  });
};

resumeTest = (req, res) => {
  const GET_CANDIDATE_ID =
    "SELECT id FROM careercounselling.candidate where name = ? AND email = ?";
  pool.query(
    GET_CANDIDATE_ID,
    [req.query.name, req.query.email],
    (err, candidate_arr) => {
      if (err) {
        return res.send(err);
      } else {
        if (candidate_arr.length === 0) {
          let data = {
            isResumeTest: false,
            resumeData: [],
            pageNumber: null,
            candidateId: null,
          };
          return res.json({
            data: data,
          });
        } else {
          GET_CANDIDATE_PAGE_NUMBER =
            "SELECT onPage FROM careercounselling.candidate where id = ?";

          pool.query(
            GET_CANDIDATE_PAGE_NUMBER,
            candidate_arr[candidate_arr.length - 1].id,
            (err, results) => {
              if (err) {
                return res.send(err);
              } else {
                const test_id = getTestIdFromPageNumber(results[0].onPage);
                if (test_id) {
                  const RESUME_TEST =
                    "select * from careercounselling.answer where test_id = ? AND candidate_id = ?";
                  pool.query(
                    RESUME_TEST,
                    [test_id, candidate_arr[candidate_arr.length - 1].id],
                    (err, resumeTestData) => {
                      if (err) {
                        return res.send(err);
                      } else {
                        let data = {
                          isResumeTest: true,
                          resumeData: resumeTestData,
                          pageNumber: results[0].onPage,
                          candidateId:
                            candidate_arr[candidate_arr.length - 1].id,
                        };
                        return res.json({
                          data: data,
                        });
                      }
                    }
                  );
                } else {
                  let data = {
                    isResumeTest: true,
                    resumeData: [],
                    pageNumber: results[0].onPage,
                    candidateId: candidate_arr[candidate_arr.length - 1].id,
                  };
                  return res.json({
                    data: data,
                  });
                }
              }
            }
          );
        }
      }
    }
  );
};
getTestIdFromPageNumber = (pageNumber) => {
  switch (pageNumber) {
    case 0:
      return false;
    case 1:
      return false;
    case 2:
      return 1;
    case 3:
      return false;
    case 4:
      return 2;
    case 5:
      return false;
    case 6:
      return 3;
    case 7:
      return false;
    case 8:
      return 4;
    case 9:
      return false;
    case 10:
      return 5;
    case 11:
      return false;
    case 12:
      return 6;
    case 13:
      return false;
    case 14:
      return 7;
    case 15:
      return false;
    case 16:
      return 8;
    case 17:
      return false;
    case 18:
      return false;
    case 19:
      return 9;
    case 20:
      return false;
    case 21:
      return 10;
    case 22:
      return false;
    case 23:
      return 11;
    case 24:
      return false;
    case 25:
      return 12;
    case 26:
      return false;
    case 27:
      return 13;
    case 28:
      return false;
    case 29:
      return 14;
    case 30:
      return false;
    case 31:
      return 15;
    case 32:
      return false;
    case 33:
      return 16;
    case 34:
      return false;
    case 35:
      return 17;
    case 36:
      return false;
    default:
      return false;
  }
};
mailResult = (req, res) => {
  let message =
    "<p>Hi,</p>" +
    "<p>Thank you for giving test with us.The result of the test is given below.We will contact you for counselling soon.</p>" +
    "<p>If you want you can contact us on following number.</p>" +
    "<p>Diwakar Thanekar - +91 9890972463</p>" +
    "<p>RESULT</p>" +
    "<h3>GATB</h3>" +
    '<table style="border: 1px solid #333;">' +
    "<thead>" +
    "<th> Test Name </th>" +
    "<th> Score </th>" +
    "<th> Stanine </th>" +
    // "<th> Remark </th>" +
    "</thead>";

  for (let i = 0; i < req.body.data.gatbDataSource.length; i++) {
    message +=
      "<tr>" +
      "<td>" +
      req.body.data.gatbDataSource[i].test_name +
      "</td>" +
      "<td>" +
      req.body.data.gatbDataSource[i].score +
      "</td>" +
      "<td>" +
      req.body.data.gatbDataSource[i].stanine +
      "</td>" +
      // "<td>" +
      // req.body.data.gatbDataSource[i].remark +
      // "</td>" +
      "</tr>";
  }

  message +=
    "</table>" +
    "<br></br>" +
    "<h3>NVTI</h3>" +
    '<table style="border: 1px solid #333;">' +
    "<thead>" +
    "<th> Test Name </th>" +
    "<th> Score </th>" +
    "<th> Stanine </th>";
  ("</thead>");

  for (let i = 0; i < req.body.data.nvtiDataSource.length; i++) {
    message +=
      "<tr>" +
      "<td>" +
      req.body.data.nvtiDataSource[i].test_name +
      "</td>" +
      "<td>" +
      req.body.data.nvtiDataSource[i].score +
      "</td>" +
      "<td>" +
      req.body.data.nvtiDataSource[i].stanine +
      "</td>" +
      "</tr>";
  }
  message +=
    "</table>" +
    "<br></br>" +
    "<h3>Interest</h3>" +
    '<table style="border: 1px solid #333;">' +
    "<thead>" +
    "<th> Test Name </th>" +
    "<th> Score </th>" +
    "<th> Stanine </th>";
  ("</thead>");

  for (let i = 0; i < req.body.data.interestDataSource.length; i++) {
    message +=
      "<tr>" +
      "<td>" +
      req.body.data.interestDataSource[i].test_name +
      "</td>" +
      "<td>" +
      req.body.data.interestDataSource[i].score +
      "</td>" +
      "<td>" +
      req.body.data.interestDataSource[i].stanine +
      "</td>" +
      "</tr>";
  }

  message +=
    "</table>" +
    "<br></br>" +
    "<h3>Adjustment</h3>" +
    '<table style="border: 1px solid #333;">' +
    "<thead>" +
    "<th> Test Name </th>" +
    "<th> Score </th>" +
    // "<th> Remark </th>";
    "</thead>";

  for (let i = 0; i < req.body.data.adjustmentDataSource.length; i++) {
    message +=
      "<tr>" +
      "<td>" +
      req.body.data.adjustmentDataSource[i].test_name +
      "</td>" +
      "<td>" +
      req.body.data.adjustmentDataSource[i].score +
      "</td>" +
      // "<td>" +
      // req.body.data.adjustmentDataSource[i].stanine +
      // "</td>" +
      "</tr>";
  }

  message += "</table>";

  message += "<h2>Thank you!!!</h2>";

  mailActions.sendMail("Result", message, req.body.data.email, res);
};

changePageToResume = (req, res) => {
  const INSERT_PAGE_NUMBER_TO_CANDIDATE =
    "UPDATE candidate set onPage = ? WHERE id = ?";
  pool.query(
    INSERT_PAGE_NUMBER_TO_CANDIDATE,
    [req.body.data.pageNumber, req.body.data.candidateId],
    (err, results) => {
      if (err) {
        console.log(err);
        return res.send(err);
      } else {
        return res.status(200).json({ isDataChanged: true });
      }
    }
  );
};
module.exports.postCandidateInfo = postCandidateInfo;
module.exports.getCandidateInfo = getCandidateInfo;
module.exports.resumeTest = resumeTest;
module.exports.mailResult = mailResult;
module.exports.changePageToResume = changePageToResume;
