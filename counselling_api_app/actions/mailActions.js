var nodemailer = require("nodemailer");
const { google } = require("googleapis");

const CLIENT_ID =
  "77578076457-erj5i2v2tmneqe1n5v37ic0u804e5t9m.apps.googleusercontent.com";
const CLEINT_SECRET = "qII5NO328uNc0VLqMBYEgf6j";
const REDIRECT_URI = "https://developers.google.com/oauthplayground";
const REFRESH_TOKEN =
  "1//04FUAKWblQNg5CgYIARAAGAQSNwF-L9IrL2ihoxJ9H3HZC6uwt5dpYxsQx9ER_SQ5wfHd0_5I3aSlXC_wg_qWUvwfW0P7lmH9dbA";

const oAuth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLEINT_SECRET,
  REDIRECT_URI
);
oAuth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

async function sendMail(subject, message, sendTo, res) {
  const accessToken = await oAuth2Client.getAccessToken();

  const transport = nodemailer.createTransport({
    service: "gmail",
    auth: {
      type: "OAuth2",
      user: "careerdishaforyou@gmail.com",
      clientId: CLIENT_ID,
      clientSecret: CLEINT_SECRET,
      refreshToken: REFRESH_TOKEN,
      accessToken: accessToken,
    },
  });

  var mailOptions = {
    from: "careerdishaforyou@gmail.com",
    to: sendTo,
    subject: subject,
    html: message,
  };

  transport.sendMail(mailOptions, function (error) {
    if (error) {
      console.log(error);
    } else {
      return res.json({
        isMailSent: true,
      });
    }
  });
}

module.exports.sendMail = sendMail;
