const express = require("express");
const cors = require("cors");
const adminroutes = require("./routes/adminRoutes");
const candidateRoutes = require("./routes/candidateRoutes");
const testRoutes = require("./routes/testRoutes");
const razorpayRoutes = require("./routes/razorpayRoutes");

const bodyparser = require("body-parser");

const app = express();

//const router = express.Router();
app.use(cors());

app.use(bodyparser.json()); // handle json data
app.use(bodyparser.urlencoded({ extended: true })); // handle URL-encoded data

app.use("/admin", adminroutes);
app.use("/candidate", candidateRoutes);
app.use("/test", testRoutes);
app.use("/razorpay", razorpayRoutes);
app.listen(process.env.PORT || 4000, () => {
  console.log("Product server listening on port 4000");
});

module.exports = app;
