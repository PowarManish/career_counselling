import { Card, Col, Divider, Row, Button } from "antd";
import Title from "antd/lib/typography/Title";
import React, { Component } from "react";
import { connect } from "react-redux";
import { getCandidateInfo } from "../../actions/candidateActions";
import * as localStorage from "../../utility/localStorage";
import jsPDF from "jspdf";
import "jspdf-autotable";

export class CandidateDetails extends Component {
  getCandidateData = (candidate_id) => {
    const { candidate } = this.props;
    var candidateData = {};
    for (var i = 0; i < candidate.length; i++) {
      if (candidate[i].id === candidate_id) {
        candidateData = candidate[i];
      }
    }
    return candidateData;
  };
  returnYesorNo = (value) => {
    if (value === 0) {
      return "No";
    } else if (value === 1) {
      return "Yes";
    } else {
      //ErrorCase
      return "NO";
    }
  };
  componentDidMount() {
    this.props.getCandidateInfo();
  }

  downloadPDF = () => {
    const candidateData = this.getCandidateData(
      localStorage.getItem("candidate_id")
    );
    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "portrait"; // portrait or landscape

    const marginLeft = 40;
    const doc = new jsPDF(orientation, unit, size);

    doc.setFontSize(17);
    doc.text("Student Details", marginLeft, 40);
    doc.setFontSize(15);
    doc.text("Basic Details", marginLeft, 70);
    doc.setFontSize(10);

    doc.text("Candidate Name :" + candidateData.name, marginLeft, 90);
    doc.text("Permanant Address :" + candidateData.address, marginLeft, 105);
    doc.text("Phone Number :" + candidateData.phone_number, marginLeft, 120);
    doc.text("School Name :" + candidateData.school_name, marginLeft, 135);
    doc.text("Standard :" + candidateData.standard, marginLeft, 150);
    doc.text(
      "Date of Birth :" +
        new Date(candidateData.date_of_birth).toLocaleDateString("en-GB"),
      marginLeft,
      165
    );
    doc.text("Age :" + candidateData.age, marginLeft, 180);
    doc.text("Mother Tongue :" + candidateData.mother_tongue, marginLeft, 195);
    doc.text("Medium :" + candidateData.medium_of_instruction, marginLeft, 210);
    doc.setFontSize(15);
    doc.text("Personal Details", marginLeft, 235);
    doc.setFontSize(10);

    doc.text("Hobby :" + candidateData.hobby, marginLeft, 255);
    doc.text("Book :" + candidateData.fav_book_type, marginLeft, 270);
    doc.text(
      "Separate Room for Study :" +
        this.returnYesorNo(candidateData.is_separate_room_available),
      marginLeft,
      285
    );
    doc.text(
      "Does have Private Tutuion :" +
        this.returnYesorNo(candidateData.is_private_tution),
      marginLeft,
      300
    );
    doc.text(
      "Subjects for private tution if any :" +
        candidateData.private_tution_subjects,
      marginLeft,
      315
    );

    doc.text(
      "Preferred Streams in order :" + candidateData.preferred_streams,
      marginLeft,
      330
    );
    doc.text(
      "Liked Subject :" + candidateData.favourite_subject,
      marginLeft,
      345
    );
    doc.text(
      "Disliked Subject :" + candidateData.disliked_subject,
      marginLeft,
      360
    );
    doc.text(
      "Parents Choice :" + candidateData.parents_preferred_stream,
      marginLeft,
      375
    );
    doc.text(
      "Your Choice :" + candidateData.candidate_preferred_stream,
      marginLeft,
      390
    );
    doc.save(candidateData.name + " Information.pdf");
  };
  render() {
    const candidateData = this.getCandidateData(
      localStorage.getItem("candidate_id")
    );
    return (
      <div>
        <Button onClick={this.downloadPDF}>Download As PDF</Button>
        <Card style={{ marginTop: 30 }}>
          <Title level={3}>Student Details</Title>
          <Title level={4}>Basic Details</Title>
          <Card>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b> Candidate Name : </b>
              </Col>
              {candidateData.name}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b>Permanant Address : </b>
              </Col>
              {candidateData.address}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b>Phone Number : </b>
              </Col>
              {candidateData.phone_number}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b>School Name :</b>
              </Col>
              {candidateData.school_name}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b>Standard :</b>
              </Col>
              {candidateData.standard}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b> Date of Birth :</b>
              </Col>
              {new Date(candidateData.date_of_birth).toLocaleDateString(
                "en-GB"
              )}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b>Age :</b>
              </Col>
              {candidateData.age}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b>Mother Tongue :</b>
              </Col>
              {candidateData.mother_tongue}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b> Medium : </b>
              </Col>
              {candidateData.medium_of_instruction}
            </Row>
          </Card>
          <Divider style={{ borderColor: "#000000" }} />
          <Card>
            <Title level={4}>Personal Details</Title>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b>Hobby :</b>
              </Col>
              {candidateData.hobby}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b>Book :</b>
              </Col>
              {candidateData.fav_book_type}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b>Separate Room for Study :</b>
              </Col>
              {this.returnYesorNo(candidateData.is_separate_room_available)}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b> Does have Private Tutuion :</b>
              </Col>
              {this.returnYesorNo(candidateData.is_private_tution)}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b> Subjects for private tution if any :</b>
              </Col>
              {candidateData.private_tution_subjects}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b> Preferred Streams in order : </b>
              </Col>
              {candidateData.preferred_streams}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b> Liked Subject :</b>
              </Col>
              {candidateData.favourite_subject}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b> Disliked Subject : </b>
              </Col>
              {candidateData.disliked_subject}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b> Parents Choice : </b>
              </Col>
              {candidateData.parents_preferred_stream}
            </Row>
            <Row style={{ margin: 5 }}>
              <Col span={8}>
                <b> Your Choice :</b>
              </Col>
              {candidateData.candidate_preferred_stream}
            </Row>
          </Card>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  candidate: state.candidateReducer.candidate,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getCandidateInfo: () => dispatch(getCandidateInfo()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CandidateDetails);
