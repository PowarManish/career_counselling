import { Card, Table, Dropdown, Menu } from "antd";
import { EllipsisOutlined } from "@ant-design/icons";
import React, { Component } from "react";
import { connect } from "react-redux";
import { getCandidateInfo } from "../../actions/candidateActions";
import * as localStorage from "../../utility/localStorage";

export class CandidateInfo extends Component {
  state = {
    columns: [
      {
        title: "Name",
        dataIndex: "name",
        key: "name",
      },
      {
        title: "School Name",
        dataIndex: "school_name",
        key: "school_name",
      },
      {
        title: "Email",
        dataIndex: "email",
        key: "email",
      },
      {
        title: "Phone Number",
        dataIndex: "phone_number",
        key: "phone_number",
        filters: [
          {
            text: "Counselling Not Done",
            value: "CounsellingNotDone",
          },
          {
            text: "Payment Not Done",
            value: "PaymentNotDone",
          },
        ],

        onFilter: (value, record) =>
          value === "CounsellingNotDone"
            ? record.isCounsellingDone === 0 ||
              record.isCounsellingDone === null
            : record.razorpay_payment_id === null,
      },

      {
        title: "Actions",
        key: "Actions",
        render: (record) => (
          <Dropdown
            overlay={
              <Menu onClick={(e) => this.handleMenuClick(e.key, record)}>
                <Menu.Item key="Information">Information</Menu.Item>
                <Menu.Item key="Result">Result</Menu.Item>
              </Menu>
            }
            trigger={["click"]}
          >
            <EllipsisOutlined />
          </Dropdown>
        ),
      },
    ],
  };
  handleMenuClick(e, record) {
    switch (e) {
      case "Information":
        localStorage.setItem("candidate_id", record.id);
        return this.props.history.replace("/admin/candidateDetails");
      case "Result":
        localStorage.setItem("candidate_id", record.id);
        return this.props.history.replace("/admin/result");
      default:
        return "DefaultStatement";
    }
  }
  componentDidMount() {
    this.props.getCandidateInfo();
  }
  render() {
    var { columns } = this.state;

    return (
      <div>
        <Card
          style={{
            marginTop: 25,
            marginBottom: 25,
          }}
        >
          <Table dataSource={this.props.candidate} columns={columns} />
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  candidate: state.candidateReducer.candidate,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getCandidateInfo: () => dispatch(getCandidateInfo()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CandidateInfo);
