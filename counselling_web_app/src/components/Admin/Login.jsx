import React, { Component } from "react";
import { Form, Input, Button, Card, message } from "antd";
import { connect } from "react-redux";
import {
  authenticateLogin,
  clearWrongAdminCredential,
} from "../../actions/adminActions";
import * as localStorage from "../../utility/localStorage";

message.config({
  top: 50,
  maxCount: 1,
});
export class Login extends Component {
  onFinish = (values) => {
    this.props.authenticateLogin(values);
  };
  messageClosed = () => {
    this.props.clearWrongAdminCredential();
  };
  componentDidUpdate() {
    if (this.props.isLoggedIn === true) {
      localStorage.setItem("isLoggedIn", this.props.isLoggedIn);
      message.success("Logged In successfully", 2);
      return this.props.history.replace(`/admin/dashboard`);
    } else if (this.props.isLoggedIn === false) {
      message.error("Wrong ID or Password", 3, this.messageClosed());
    }
  }

  render() {
    // if (this.props.isLoggedIn) {
    //   return <Redirect to="/admin/dashboard" />;
    // } else
    if (this.props.match.isExact) {
      return (
        <Card style={{ width: 500, left: 450, top: 150, position: "absolute" }}>
          <Form
            name="basic"
            initialValues={{ remember: true }}
            onFinish={this.onFinish}
          >
            <Form.Item
              label="Username"
              name="username"
              rules={[
                { required: true, message: "Please input your username!" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Card>
      );
    } else {
      return (
        <div>
          {/* {this.props.routes.map((route) => (
            <RouteWithSubRoutes key={route.path} {...route} />
          ))} */}
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  isLoggedIn: state.adminReducer.isLoggedIn,
});

const mapDispatchToProps = (dispatch) => {
  return {
    authenticateLogin: (params) => dispatch(authenticateLogin(params)),
    clearWrongAdminCredential: () => dispatch(clearWrongAdminCredential()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
