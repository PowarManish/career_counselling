import React, { Component } from "react";
import { Card, Col, Row, Statistic } from "antd";
import { connect } from "react-redux";
import { getCandidateInfo } from "../../actions/candidateActions";
import moment from "moment";

export class Dashboard extends Component {
  componentDidMount() {
    this.props.getCandidateInfo();
  }

  getRemainingCount = () => {
    let count = 0;
    for (let i = 0; i < this.props.candidate.length; i++) {
      if (this.props.candidate[i].isCounsellingDone === 0) {
        count = count + 1;
      }
    }
    return count;
  };
  getNumberofTestsThisMonth = () => {
    let count = 0;
    for (let i = 0; i < this.props.candidate.length; i++) {
      if (
        new Date(this.props.candidate[i].created_on).getMonth() ===
        new Date().getMonth()
      ) {
        count = count + 1;
      }
    }
    return count;
  };

  getNumberofTestsThisWeek = () => {
    let count = 0;
    for (let i = 0; i < this.props.candidate.length; i++) {
      if (
        moment(this.props.candidate[i].created_on).weeks() ===
        moment(new Date()).weeks()
      ) {
        count = count + 1;
      }
    }
    return count;
  };

  getNumberofTestsThisYear = () => {
    let count = 0;
    for (let i = 0; i < this.props.candidate.length; i++) {
      if (
        new Date(this.props.candidate[i].created_on).getFullYear() ===
        new Date().getFullYear()
      ) {
        count = count + 1;
      }
    }
    return count;
  };

  render() {
    return (
      <div>
        <div className="site-statistic-demo-card">
          <Row gutter={16} style={{ margin: 10 }}>
            <Col span={8}>
              <Card>
                <Statistic
                  title={<b>Total Candidates</b>}
                  value={this.props.candidate.length}
                  valueStyle={{ color: "#3f8600" }}
                />
              </Card>
            </Col>
            <Col span={8}>
              <Card>
                <Statistic
                  title={<b>Counselling Remaining</b>}
                  value={this.getRemainingCount()}
                  valueStyle={{ color: "#cf1322" }}
                />
              </Card>
            </Col>
          </Row>
          <Row gutter={16} style={{ margin: 10 }}>
            <Col span={8}>
              <Card>
                <Statistic
                  title={<b>Tests in this week</b>}
                  value={this.getNumberofTestsThisWeek()}
                  valueStyle={{ color: "#3f8600" }}
                />
              </Card>
            </Col>
            <Col span={8}>
              <Card>
                <Statistic
                  title={<b>Tests in this month</b>}
                  value={this.getNumberofTestsThisMonth()}
                  valueStyle={{ color: "#3f8600" }}
                />
              </Card>
            </Col>
          </Row>
          <Row gutter={16} style={{ margin: 10 }}>
            <Col span={8}>
              <Card>
                <Statistic
                  title={<b>Tests in this Year</b>}
                  value={this.getNumberofTestsThisYear()}
                  valueStyle={{ color: "#3f8600" }}
                />
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  candidate: state.candidateReducer.candidate,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getCandidateInfo: () => dispatch(getCandidateInfo()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
