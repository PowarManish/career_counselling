import {
  Card,
  Table,
  Input,
  Button,
  Row,
  Col,
  Form,
  Divider,
  Modal,
} from "antd";
import Title from "antd/lib/typography/Title";
import React, { Component } from "react";
import { connect } from "react-redux";
import { getResult, getAdjustmentAnswers } from "../../actions/testActions";
import { getCandidateInfo } from "../../actions/candidateActions";
import {
  postCounsellingConclusion,
  mailCounsellingConclusion,
  clearConclusionPosted,
  clearConclusionMailSent,
} from "../../actions/adminActions";
import * as localStorage from "../../utility/localStorage";
import {
  getCandidateNameFromCandidateID,
  getConclusionFromCandidateID,
} from "../../utility/generalFunctions";
import jsPDF from "jspdf";
import "jspdf-autotable";

const { TextArea } = Input;

const columns = [
  {
    title: "Test Name",
    dataIndex: "test_name",
    key: "test_name",
  },
  {
    title: "Score",
    dataIndex: "score",
    key: "score",
  },
  {
    title: "Stanine",
    dataIndex: "stanine",
    key: "Stanine",
  },
];

const gatb_columns = [
  {
    title: "Test Name",
    dataIndex: "test_name",
    key: "test_name",
  },
  {
    title: "Score",
    dataIndex: "score",
    key: "score",
  },
  {
    title: "Stanine",
    dataIndex: "stanine",
    key: "Stanine",
  },
  {
    title: "Remark",
    dataIndex: "remark",
    key: "Remark",
  },
];
const adjustment_columns = [
  {
    title: "Test Name",
    dataIndex: "test_name",
    key: "test_name",
  },
  {
    title: "Score",
    dataIndex: "score",
    key: "score",
  },
  {
    title: "Remark",
    dataIndex: "stanine",
    key: "Stanine",
  },
];
const adjustmentAns_columns = [
  {
    title: "Question",
    dataIndex: "question_number",
    key: "question_number",
  },
  {
    title: "Answer",
    dataIndex: "selected_answer",
    key: "selected_answer",
  },
];

export class Result extends Component {
  state = {
    adjustmentModal: false,
  };
  componentDidMount() {
    this.props.getCandidateInfo();
    this.props.getResult({
      candidate_id: localStorage.getItem("candidate_id"),
    });
    this.props.getAdjustmentAnswers({
      candidate_id: localStorage.getItem("candidate_id"),
    });
  }
  componentDidUpdate() {
    if (
      this.props.isConclusionPosted === true &&
      this.props.isConclusionMailSent === true
    ) {
      this.props.clearConclusionPosted();
      this.props.clearConclusionMailSent();
      localStorage.removeItem("candidate_id");
      this.props.history.replace("/admin/viewCandidate");
    }
  }
  OpenAdjustmentModal = () => {
    this.setState({ adjustmentModal: true });
  };
  closeModal = () => {
    this.setState({ adjustmentModal: false });
  };
  getDatasourceforGATB = () => {
    var GATB = [];
    for (let i = 0; i < this.props.result.length; i++) {
      if (
        ["NC", "AC", "TDS", "VE", "TM", "AR", "FM", "NVR", "VM", "VH"].includes(
          this.props.result[i].test_name
        )
      ) {
        GATB.push(this.props.result[i]);
      }
    }
    return GATB;
  };
  getDatasourceforInterest = () => {
    var interest = [];
    for (let i = 0; i < this.props.result.length; i++) {
      if (
        ["Medical", "Engineering", "Commerce", "Arts", "Fine Arts"].includes(
          this.props.result[i].test_name
        )
      ) {
        interest.push(this.props.result[i]);
      }
    }
    return interest;
  };
  getDatasourceforAdjustment = () => {
    var adjustment = [];
    for (let i = 0; i < this.props.result.length; i++) {
      if (
        ["Home", "Health", "Social", "Emotion"].includes(
          this.props.result[i].test_name
        )
      ) {
        adjustment.push(this.props.result[i]);
      }
    }
    return adjustment;
  };
  getDatasourceforNVTI = () => {
    var nvti = [];
    for (let i = 0; i < this.props.result.length; i++) {
      if (
        this.props.result[i].test_name === "NVTI" ||
        this.props.result[i].test_name === "Reflection"
      ) {
        nvti.push(this.props.result[i]);
      }
    }
    return nvti;
  };

  submitConclusion = (values) => {
    var candidateEMailAddress = "";
    var gatbDataSource = [];
    var interestDataSource = [];
    var adjustmentDataSource = [];
    var nvtiDataSource = [];

    for (let i = 0; i < this.props.candidate.length; i++) {
      if (localStorage.getItem("candidate_id") === this.props.candidate[i].id) {
        candidateEMailAddress = this.props.candidate[i].email;
        gatbDataSource = this.getDatasourceforGATB();
        interestDataSource = this.getDatasourceforInterest();
        adjustmentDataSource = this.getDatasourceforAdjustment();
        nvtiDataSource = this.getDatasourceforNVTI();
      }
    }
    var mailData = {
      email: candidateEMailAddress,
      gatbDataSource: gatbDataSource,
      interestDataSource: interestDataSource,
      adjustmentDataSource: adjustmentDataSource,
      nvtiDataSource: nvtiDataSource,
      conclusion: values.conclusion,
    };

    let data = {
      candidateId: localStorage.getItem("candidate_id"),
      values: values,
    };
    this.props.postCounsellingConclusion(data);
    this.props.mailCounsellingConclusion(mailData);
  };
  getInitialvalue = () => {
    return getConclusionFromCandidateID(
      this.props.candidate,
      localStorage.getItem("candidate_id")
    );
  };
  downloadPDF = () => {
    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "portrait"; // portrait or landscape

    const marginLeft = 40;
    const doc = new jsPDF(orientation, unit, size);

    doc.setFontSize(15);
    doc.text("Result", marginLeft, 40);
    doc.text(
      "Candidate Name :" +
        getCandidateNameFromCandidateID(
          this.props.candidate,
          localStorage.getItem("candidate_id")
        ),
      marginLeft,
      60
    );

    doc.text("GATB", marginLeft, 90);
    doc.autoTable({
      head: [["Test Name", "Score", "Stanine", "Remark"]],
      body: this.getDatasourceforGATB().map((record) => [
        record.test_name,
        record.score,
        record.stanine,
        record.remark,
      ]),
      startY: 110,
    });
    doc.text("NVTI", marginLeft, doc.autoTable.previous.finalY + 40);
    doc.autoTable({
      head: [["Test Name", "Score", "Stanine"]],
      body: this.getDatasourceforNVTI().map((record) => [
        record.test_name,
        record.score,
        record.stanine,
      ]),
      startY: doc.lastAutoTable.finalY + 60,
    });
    doc.text(
      "Interest Inventory",
      marginLeft,
      doc.autoTable.previous.finalY + 40
    );
    doc.autoTable({
      head: [["Test Name", "Score", "Stanine"]],
      body: this.getDatasourceforInterest().map((record) => [
        record.test_name,
        record.score,
        record.stanine,
      ]),
      startY: doc.lastAutoTable.finalY + 60,
    });
    doc.text("Adjustment", marginLeft, doc.autoTable.previous.finalY + 40);
    doc.autoTable({
      head: [["Test Name", "Score", "Remarks"]],
      body: this.getDatasourceforAdjustment().map((record) => [
        record.test_name,
        record.score,
        record.stanine,
      ]),
      startY: doc.lastAutoTable.finalY + 60,
    });
    if (
      getConclusionFromCandidateID(
        this.props.candidate,
        localStorage.getItem("candidate_id")
      ) !== null
    ) {
      doc.text(
        "Conclusion: \n" +
          getConclusionFromCandidateID(
            this.props.candidate,
            localStorage.getItem("candidate_id")
          ),
        marginLeft,
        doc.autoTable.previous.finalY + 25
      );
    }
    doc.save(
      getCandidateNameFromCandidateID(
        this.props.candidate,
        localStorage.getItem("candidate_id")
      ) + " Result.pdf"
    );
  };
  render() {
    return (
      <div>
        <Button onClick={this.downloadPDF}>Download As PDF</Button>
        <div className="parentScreen">
          <Title level={3}>
            Candidate Name: {""}
            {getCandidateNameFromCandidateID(
              this.props.candidate,
              localStorage.getItem("candidate_id")
            )}
          </Title>
          <div className="splitScreen">
            <Card>
              <Title>GATB</Title>
              <Table
                dataSource={this.getDatasourceforGATB()}
                columns={gatb_columns}
                pagination={{ hideOnSinglePage: true }}
                size="small"
                rowKey="id"
                style={{ margin: 5 }}
              />
              <Title>NVTI</Title>
              <Table
                dataSource={this.getDatasourceforNVTI()}
                columns={columns}
                pagination={{ hideOnSinglePage: true }}
                size="small"
                rowKey="id"
                style={{ margin: 5 }}
              />
            </Card>
          </div>
          <div className="splitScreen">
            <Card>
              <Title>Interest Inventory</Title>
              <Table
                dataSource={this.getDatasourceforInterest()}
                columns={columns}
                pagination={{ hideOnSinglePage: true }}
                size="small"
                rowKey="id"
                style={{ margin: 5 }}
              />
              <Title>Adjustment Test</Title>
              <Table
                dataSource={this.getDatasourceforAdjustment()}
                columns={adjustment_columns}
                pagination={{ hideOnSinglePage: true }}
                size="small"
                rowKey="id"
                style={{ margin: 5 }}
              />
              <Button onClick={this.OpenAdjustmentModal}>
                See Adjustment Ans
              </Button>
              <Modal
                title="Adjustment Answers"
                visible={this.state.adjustmentModal}
                onCancel={this.closeModal}
                footer={[
                  <Button key="back" onClick={this.closeModal}>
                    Close
                  </Button>,
                ]}
              >
                <Table
                  dataSource={this.props.adjustmentAnswers}
                  columns={adjustmentAns_columns}
                  pagination={{ hideOnSinglePage: true }}
                  size="small"
                  rowKey="question_number"
                />
              </Modal>
            </Card>
          </div>
        </div>
        <Divider style={{ borderColor: "#000000" }} />
        {getConclusionFromCandidateID(
          this.props.candidate,
          localStorage.getItem("candidate_id")
        ) === null ? (
          <Form
            style={{
              marginTop: 10,
            }}
            onFinish={this.submitConclusion}
            initialValues={{
              conclusion: this.getInitialvalue(),
            }}
          >
            <Row>
              <Col span={12}>
                <Form.Item
                  label="Conclusion"
                  name="conclusion"
                  rules={[
                    { required: true, message: "Please input conclusion!" },
                  ]}
                >
                  <TextArea />
                </Form.Item>
              </Col>
            </Row>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        ) : (
          <Card style={{ width: 400 }}>
            <h3>Conclusion</h3>

            {getConclusionFromCandidateID(
              this.props.candidate,
              localStorage.getItem("candidate_id")
            )}
          </Card>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  result: state.testReducer.result,
  candidate: state.candidateReducer.candidate,
  isConclusionPosted: state.adminReducer.isConclusionPosted,
  isConclusionMailSent: state.adminReducer.isConclusionMailSent,
  adjustmentAnswers: state.testReducer.adjustmentAnswers,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getResult: (params) => dispatch(getResult(params)),
    getAdjustmentAnswers: (params) => dispatch(getAdjustmentAnswers(params)),
    getCandidateInfo: () => dispatch(getCandidateInfo()),
    postCounsellingConclusion: (values) =>
      dispatch(postCounsellingConclusion(values)),
    mailCounsellingConclusion: (values) =>
      dispatch(mailCounsellingConclusion(values)),
    clearConclusionPosted: () => dispatch(clearConclusionPosted()),
    clearConclusionMailSent: () => dispatch(clearConclusionMailSent()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Result);
