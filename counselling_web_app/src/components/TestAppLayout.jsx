import React from "react";
import { Layout, Menu } from "antd";
import RouteWithSubRoutes from "./common/RouteWithSubRoutes";
import Landing from "./test/Landing";
import { LocaleContext } from "../context/LocaleContext";
import { IntlProvider } from "react-intl";
import en from "../locale/en.json";
import mr from "../locale/mr.json";
import { Redirect } from "react-router-dom";

const { Header, Footer, Content } = Layout;

const TestAppLayout = (props) => {
  const [locale, setLocale] = React.useContext(LocaleContext);
  const nextLocale = locale === "en" ? "mr" : "en";
  const nestedMessages = { en, mr };
  const flattenMessages = (nestedMessages, prefix = "") => {
    return Object.keys(nestedMessages).reduce((messages, key) => {
      let value = nestedMessages[key];
      let prefixedKey = prefix ? `${prefix}.${key}` : key;

      if (typeof value === "string") {
        messages[prefixedKey] = value;
      } else {
        Object.assign(messages, flattenMessages(value, prefixedKey));
      }

      return messages;
    }, {});
  };
  let messages = flattenMessages(nestedMessages[locale]);

  if (!props.location.pathname.includes("/admin")) {
    return (
      <IntlProvider locale={locale} messages={messages}>
        <Layout>
          {/* <Header>Header</Header> */}
          {!(
            props.location.pathname === "/" ||
            props.location.pathname === "/result" ||
            props.location.pathname === "/candidateInfo" ||
            props.location.pathname === "/resume" ||
            props.location.pathname === "/payment"
          ) && (
            <Menu
              style={{
                textAlign: "right",
                position: "fixed",
                zIndex: 1,
                width: "100%",
              }}
              onClick={() => setLocale(nextLocale)}
              mode="horizontal"
            >
              <Menu.Item>
                Change language to {nextLocale === "en" ? "English" : "Marathi"}
              </Menu.Item>
            </Menu>
          )}
          <Content
            style={{
              minHeight: "100vh",
              marginTop: 75,
              marginRight: 15,
              marginLeft: 15,
            }}
          >
            <Landing />

            {localStorage.getItem("payment_id") ||
            (props.location.pathname === "/payment" &&
              localStorage.getItem("candidate_id")) ||
            props.location.pathname === "/candidateInfo" ||
            props.location.pathname === "/resume" ? (
              props.routes.map((route) => (
                <RouteWithSubRoutes key={route.path} {...route} />
              ))
            ) : (
              <Redirect
                to={{
                  pathname: "/",
                }}
              />
            )}
          </Content>
          <Footer></Footer>
        </Layout>
      </IntlProvider>
    );
  } else {
    return null;
  }
};

export default TestAppLayout;
