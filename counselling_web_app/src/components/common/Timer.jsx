import React, { Component } from "react";
import { Alert, notification } from "antd";

export class Timer extends Component {
  componentDidMount() {
    var intervalId = setInterval(this.timer, 1000);
    this.setState({
      intervalId: intervalId,
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  timer = () => {
    var newCount = this.props.remainingSeconds - 1;
    if (newCount >= 0) {
      this.props.setRemainingSeconds(newCount);
    } else {
      clearInterval(this.state.intervalId);
      this.props.finish();
    }
  };

  giveNotification = () => {
    notification.open({
      message: "1 min remaining",
      description:
        "Last minite remaining.After 1 min test will automatically submitted",
    });
  };

  render() {
    var min = Math.floor(this.props.remainingSeconds / 60);
    var sec = this.props.remainingSeconds % 60;
    if (this.props.remainingSeconds === 60) {
      this.giveNotification();
    }
    return (
      <Alert
        style={{
          position: "fixed",
          zIndex: 1,
          right: 20,
        }}
        message={`Minutes: ${min} Seconds:${sec}`}
        type="info"
        showIcon
      />
    );
  }
}

export default Timer;
