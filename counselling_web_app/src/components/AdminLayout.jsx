import { Breadcrumb, Layout, Menu } from "antd";
import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import RouteWithSubRoutes from "./common/RouteWithSubRoutes";
import { connect } from "react-redux";
import { logout } from "../actions/adminActions";
import * as localStorage from "../utility/localStorage";

const { Header, Footer, Content, Sider } = Layout;

export class AdminLayout extends Component {
  state = {
    collapsed: false,
  };

  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  };

  logout = () => {
    this.props.history.replace("/admin/login");
    this.props.logout();
    localStorage.removeItem("isLoggedIn");
  };
  render() {
    const { collapsed } = this.state;
    if (this.props.location.pathname !== "/admin/login") {
      return (
        <Layout style={{ minHeight: "100vh" }}>
          <Sider collapsible collapsed={collapsed} onCollapse={this.onCollapse}>
            <div className="logo" />
            <Menu theme="dark" mode="inline" style={{ marginTop: 40 }}>
              <Menu.Item key="1">
                Dashboard
                <Link to="/admin/dashboard" />
              </Menu.Item>
              <Menu.Item key="2">
                Candidate List
                <Link to="/admin/viewCandidate" />
              </Menu.Item>
              <Menu.Item
                key="10"
                onClick={this.logout}
                style={{ marginTop: 375 }}
              >
                Logout
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout className="site-layout">
            <Header className="site-layout-background" style={{ padding: 0 }} />
            <Content style={{ margin: "0 16px" }}>
              <Breadcrumb style={{ margin: "16px 0" }}>
                {/* <Breadcrumb.Item>User</Breadcrumb.Item>
                <Breadcrumb.Item>Bill</Breadcrumb.Item> */}
              </Breadcrumb>
              <div
                className="site-layout-background"
                style={{ padding: 24, minHeight: 360 }}
              >
                {localStorage.getItem("isLoggedIn") ? (
                  this.props.routes.map((route) => (
                    <RouteWithSubRoutes key={route.path} {...route} />
                  ))
                ) : (
                  <Redirect
                    to={{
                      pathname: "/admin/login",
                    }}
                  />
                )}
              </div>
            </Content>
            {/* <Footer style={{ textAlign: "center" }}>
              Ant Design ©2018 Created by Ant UED
            </Footer> */}
          </Layout>
        </Layout>
      );
    } else {
      return (
        <Content style={{ margin: "0 16px" }}>
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 360 }}
          >
            {this.props.routes.map((route) => (
              <RouteWithSubRoutes key={route.path} {...route} />
            ))}
          </div>
        </Content>
      );
    }
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logout()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminLayout);
