import { Button, Card, Table, Image } from "antd";
import Title from "antd/lib/typography/Title";
import React, { Component } from "react";
import { connect } from "react-redux";
import { getResult, sendMail, clearMailSent } from "../actions/testActions";
import { getCandidateInfo } from "../actions/candidateActions";
import ResultGraph from "../images/ResultGraph.png";
import * as localStorage from "../utility/localStorage";
const columns = [
  {
    title: "Test Name",
    dataIndex: "test_name",
    key: "test_name",
  },
  {
    title: "Score",
    dataIndex: "score",
    key: "score",
  },
  {
    title: "Stanine",
    dataIndex: "stanine",
    key: "Stanine",
  },
];
const gatb_columns = [
  {
    title: "Test Name",
    dataIndex: "test_name",
    key: "test_name",
  },
  {
    title: "Score",
    dataIndex: "score",
    key: "score",
  },
  {
    title: "Stanine",
    dataIndex: "stanine",
    key: "Stanine",
  },
];
const adjustment_columns = [
  {
    title: "Test Name",
    dataIndex: "test_name",
    key: "test_name",
  },
  {
    title: "Score",
    dataIndex: "score",
    key: "score",
  },
];
export class result extends Component {
  componentDidMount() {
    this.props.getCandidateInfo();
    this.props.getResult({
      candidate_id: localStorage.getItem("candidate_id"),
    });
  }

  getDatasourceforGATB = () => {
    var GATB = [];
    for (let i = 0; i < this.props.result.length; i++) {
      if (
        ["NC", "AC", "TDS", "VE", "TM", "AR", "FM", "NVR", "VM", "VH"].includes(
          this.props.result[i].test_name
        )
      ) {
        GATB.push(this.props.result[i]);
      }
    }
    return GATB;
  };

  getDatasourceforInterest = () => {
    var interest = [];
    for (let i = 0; i < this.props.result.length; i++) {
      if (
        ["Medical", "Engineering", "Commerce", "Arts", "Fine Arts"].includes(
          this.props.result[i].test_name
        )
      ) {
        interest.push(this.props.result[i]);
      }
    }
    return interest;
  };

  getDatasourceforAdjustment = () => {
    var adjustment = [];
    for (let i = 0; i < this.props.result.length; i++) {
      if (
        ["Home", "Health", "Social", "Emotion"].includes(
          this.props.result[i].test_name
        )
      ) {
        adjustment.push(this.props.result[i]);
      }
    }
    return adjustment;
  };

  getDatasourceforNVTI = () => {
    var nvti = [];
    for (let i = 0; i < this.props.result.length; i++) {
      if (this.props.result[i].test_name === "NVTI") {
        nvti.push(this.props.result[i]);
      }
    }
    return nvti;
  };

  endTest = () => {
    var candidateEMailAddress = "";
    var gatbDataSource = [];
    var interestDataSource = [];
    var adjustmentDataSource = [];
    var nvtiDataSource = [];

    for (let i = 0; i < this.props.candidate.length; i++) {
      if (localStorage.getItem("candidate_id") === this.props.candidate[i].id) {
        candidateEMailAddress = this.props.candidate[i].email;
        gatbDataSource = this.getDatasourceforGATB();
        interestDataSource = this.getDatasourceforInterest();
        adjustmentDataSource = this.getDatasourceforAdjustment();
        nvtiDataSource = this.getDatasourceforNVTI();
      }
    }
    var values = {
      email: candidateEMailAddress,
      gatbDataSource: gatbDataSource,
      interestDataSource: interestDataSource,
      adjustmentDataSource: adjustmentDataSource,
      nvtiDataSource: nvtiDataSource,
    };
    this.props.sendMail(values);
  };

  componentDidUpdate() {
    if (this.props.isMailSent === true) {
      this.props.clearMailSent();
      localStorage.removeItem("candidate_id");
      localStorage.removeItem("payment_id");
      this.props.history.replace("/");
    }
  }
  render() {
    return (
      <div>
        <Image width={300} src={ResultGraph}></Image>
        <div className="parentScreen">
          <div className="splitScreen">
            <Card>
              <Title>GATB</Title>
              <Table
                dataSource={this.getDatasourceforGATB()}
                columns={gatb_columns}
                pagination={{ hideOnSinglePage: true }}
                size="small"
                rowKey="id"
              />
              <Title>NVTI</Title>
              <Table
                dataSource={this.getDatasourceforNVTI()}
                columns={columns}
                pagination={{ hideOnSinglePage: true }}
                size="small"
                rowKey="id"
              />
            </Card>
          </div>
          <div className="splitScreen">
            <Card>
              <Title>Interest Inventory</Title>
              <Table
                dataSource={this.getDatasourceforInterest()}
                columns={columns}
                pagination={{ hideOnSinglePage: true }}
                size="small"
                rowKey="id"
              />
              <Title>Adjustment Test</Title>
              <Table
                dataSource={this.getDatasourceforAdjustment()}
                columns={adjustment_columns}
                pagination={{ hideOnSinglePage: true }}
                size="small"
                rowKey="id"
              />
            </Card>
          </div>
        </div>

        <Button style={{ margin: 5 }} type="primary" onClick={this.endTest}>
          End Test
        </Button>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  result: state.testReducer.result,
  candidate: state.candidateReducer.candidate,
  isMailSent: state.candidateReducer.isMailSent,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getResult: (params) => dispatch(getResult(params)),
    sendMail: (data) => dispatch(sendMail(data)),
    getCandidateInfo: () => dispatch(getCandidateInfo()),
    clearMailSent: () => dispatch(clearMailSent()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(result);
