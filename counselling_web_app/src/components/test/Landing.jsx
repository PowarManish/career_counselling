import React from "react";
import firstVectorGraphic from "../../images/slider-icon.png";
import leftImage from "../../images/left-image.png";
import { withRouter } from "react-router-dom";
import { Button, Collapse, Popover, Modal, Card } from "antd";
import { CaretRightOutlined } from "@ant-design/icons";
import logo from "../../images/Logo.jpg";

const { Panel } = Collapse;
const content = (
  <div>
    <p>
      <b>
        If you have already paid fees and started this test and it has abruptly
        closed then click this button.
      </b>
    </p>
  </div>
);

const Landing = (props) => {
  return (
    <>
      {/* <!-- ***** Preloader Start ***** --> */}
      <div id="preloader">
        <div className="jumper">
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
      {/* <!-- ***** Preloader End ***** --> */}

      {props.match.isExact && (
        <>
          <header className="header-area header-sticky">
            <div className="container">
              <div className="row">
                <div className="col-12">
                  <nav className="main-nav">
                    {/* <!-- ***** Logo Start ***** --> */}
                    <a href="/" className="logo">
                      <img src={logo} alt="Logo" />
                    </a>
                    {/* <!-- ***** Logo End ***** --> */}
                    {/* <!-- ***** Menu Start ***** --> */}
                    <ul className="nav">
                      <li className="scroll-to-section">
                        <a href="#welcome" className="active">
                          Home
                        </a>
                      </li>
                      <li className="scroll-to-section">
                        <a href="#about">About Test</a>
                      </li>
                      <li className="scroll-to-section">
                        <a href="#frequently-question">
                          Frequently Asked Questions
                        </a>
                      </li>
                    </ul>
                    <a className="menu-trigger">
                      <span>Menu</span>
                    </a>
                    {/* <!-- ***** Menu End ***** --> */}
                  </nav>
                </div>
              </div>
            </div>
          </header>

          <div className="welcome-area" id="welcome">
            {/* <!-- ***** Header Text Start ***** --> */}
            <div className="header-text">
              <div className="container">
                <div className="row">
                  <div
                    className="left-text col-lg-6 col-md-6 col-sm-12 col-xs-12"
                    data-scroll-reveal="enter left move 30px over 0.6s after 0.4s"
                  >
                    <h1>Welcome to Career Disha For You</h1>
                    <p>Choose your best career path with us.</p>
                    <a href="/candidateInfo" className="main-button-slider">
                      Start Test
                    </a>{" "}
                    <Popover content={content} title="Resume Test">
                      <a href="/resume" className="main-button-slider">
                        Resume Test
                      </a>
                    </Popover>
                  </div>
                  <div
                    className="col-lg-6 col-md-6 col-sm-12 col-xs-12"
                    data-scroll-reveal="enter right move 30px over 0.6s after 0.4s"
                  >
                    <img
                      src={firstVectorGraphic}
                      className="rounded img-fluid d-block mx-auto"
                      alt="First Vector Graphic"
                    />
                  </div>
                </div>
              </div>
            </div>
            {/* <!-- ***** Header Text End ***** --> */}
          </div>

          <section className="section" id="about">
            <div className="container">
              <div className="row">
                <div
                  className="col-lg-7 col-md-12 col-sm-12"
                  data-scroll-reveal="enter left move 30px over 0.6s after 0.4s"
                >
                  <img
                    src={leftImage}
                    className="rounded img-fluid d-block mx-auto"
                    alt="App"
                  />
                </div>
                <div className="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                  <div className="left-heading">
                    <h5>About Test</h5>
                  </div>
                  <div className="left-text">
                    <p>
                      We conduct 4 different tests.
                      <br />
                      <b>1.General Aptitude Test Battery (GATB)</b>
                      <br />
                      This aptitude test helps you to assess a suitable
                      occupation. There are 9 different subtests under this test
                      and each one is necessary to evaluate the correct result
                      of this test. <br />
                      <b> 2. Non Verbal Test of Intelligence (NVTI) </b> <br />
                      This test will check your visual reasoning skills. This
                      test includes 4 subtests where each one is necessary to
                      evaluate the correct result of this test.
                      <br />
                      <b> 3.Interest Inventory</b>
                      <br />
                      This test will determine your preferences for specific
                      fields like arts, commerce, etc.
                      <br />
                      <b> 4.Adjustment Test</b>
                      <br />
                      This psychological test helps to study different factors
                      affecting the balance of your mind.
                    </p>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <div className="hr"></div>
                </div>
              </div>
            </div>
          </section>

          <section className="section" id="frequently-question">
            <div className="container">
              {/* <!-- ***** Section Title Start ***** --> */}
              <div className="row">
                <div className="col-lg-12">
                  <div className="section-heading">
                    <h2>Frequently Asked Questions</h2>
                  </div>
                </div>
                <div className="offset-lg-3 col-lg-6">
                  <div className="section-heading">
                    <p>
                      Read the instructions given below. Take a look at some
                      frequently asked questions. If you have any further
                      queries you can reach out to us.
                    </p>
                  </div>
                </div>
              </div>
              {/* <!-- ***** Section Title End ***** --> */}
              <div className="row">
                <div className="left-text col-lg-6 col-md-6 col-sm-12">
                  <h5>Instructions before starting the test</h5>
                  <div className="accordion-text">
                    <p>
                      1. Do not use mobile phones to solve the test. Use only a
                      laptop/desktop.
                      <br />
                      2. Please carry a rough paper as it may require for
                      solving the test.
                      <br />
                      3. Once you submit the test, it cannot be further edited.
                      tests. <br />
                      4. Each test needs to complete within the stipulated time
                      limit.
                      <br />
                      5. Kindly note, all tests are compulsory.
                      <br />
                      6. Do not take help from parents/friends while giving
                      tests.
                      <br />
                    </p>
                    <Card
                      bordered={true}
                      style={{ width: 350, marginTop: 10, marginBottom: 10 }}
                      title="Introduction"
                    >
                      <iframe
                        title="Title"
                        src="https://www.youtube.com/embed/W-He5vvFiuo"
                        frameborder="10"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture;fullscreen"
                      ></iframe>
                    </Card>
                    <span>
                      Email: diwakarthanekar@gmail.com
                      <br />
                      Phone Number:+91 9890972463
                      <br />
                    </span>

                    {/* <a href="#contact-us" className="main-button">
                      Contact Us
                    </a> */}
                  </div>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-12">
                  <Collapse
                    accordion
                    expandIcon={({ isActive }) => (
                      <CaretRightOutlined rotate={isActive ? 90 : 0} />
                    )}
                    className="accordions"
                  >
                    <Panel
                      header="What is the fee for the tests and counselling?"
                      key="1"
                      className="accordion-head"
                      style={{ padding: 10, fontWeight: "bold" }}
                    >
                      <p style={{ fontWeight: "normal" }}>
                        The total cost for the test and counselling is Rs. 1000.
                        You need to pay the fees via an online payment portal.
                      </p>
                    </Panel>
                    <Panel
                      header="How does the counselling process work?"
                      key="2"
                      className="accordion-head"
                      style={{ padding: 10, fontWeight: "bold" }}
                    >
                      <p style={{ fontWeight: "normal" }}>
                        Counselling will be done within a week from the
                        submission date of the test. Counselling can be done
                        either in person or virtual through a digital platform.
                      </p>
                    </Panel>
                    <Panel
                      header="How much time is required to complete all the tests?"
                      key="3"
                      className="accordion-head"
                      style={{ padding: 10, fontWeight: "bold" }}
                    >
                      <p style={{ fontWeight: "normal" }}>
                        It will take around 3 hours to complete all the tests.
                      </p>
                    </Panel>
                    <Panel
                      header="Can we use mobile to solve tests?"
                      key="4"
                      className="accordion-head"
                      style={{ padding: 10, fontWeight: "bold" }}
                    >
                      <p style={{ fontWeight: "normal" }}>
                        Please use only a laptop/desktop to solve all the tests.
                        The use of mobile for solving the test will be tedious
                        and can affect the results.
                      </p>
                    </Panel>
                  </Collapse>
                </div>
              </div>
              <a href="/candidateInfo" className="main-button-slider">
                Start Test
              </a>
            </div>
          </section>

          <footer>
            <div className="container">
              <div className="row">
                <div className="col-lg-7 col-md-12 col-sm-12"></div>
                <div className="col-lg-5 col-md-12 col-sm-12">
                  <ul className="social">
                    <li>
                      <a
                        href="https://www.facebook.com/Career-disha-for-you-101371338463156/?ti=as"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <i className="fa fa-facebook"></i>
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://twitter.com/career_disha_"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <i className="fa fa-twitter"></i>
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://instagram.com/careerdishaforyou?igshid=mz1og6ipt4le"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <i className="fa fa-instagram"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="fa fa-linkedin"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </footer>
        </>
      )}
    </>
  );
};

export default withRouter(Landing);
