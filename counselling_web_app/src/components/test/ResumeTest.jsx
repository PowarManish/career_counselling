import { Button, Card, Form, Input, message } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  resumeTest,
  clearWrongResumeCredential,
  getCandidateInfo,
} from "../../actions/candidateActions";

export class ResumeTest extends Component {
  onFinish = (values) => {
    this.props.resumeTest({
      name: values.name,
      email: values.email,
    });
  };

  getLocalStorageData = () => {
    var selectedAns = [];
    if (this.props.resumeTestData.length === 0) {
      return null;
    }
    for (let i = 0; i < this.props.resumeTestData.length; i++) {
      selectedAns.push({
        questionNo: this.props.resumeTestData[i].question_number,
        answer: this.props.resumeTestData[i].selected_answer,
        remainingSeconds: this.props.resumeTestData[i].remaining_time,
      });
    }
    var localStorageData = JSON.stringify({
      page: Math.ceil(
        this.props.resumeTestData[this.props.resumeTestData.length - 1]
          .question_number / 5
      ),
      selectedAns: selectedAns,
      remainingSeconds: this.props.resumeTestData[
        this.props.resumeTestData.length - 1
      ].remaining_time,
    });
    return localStorageData;
  };

  redirectToContinueTest = () => {
    localStorage.setItem("candidate_id", this.props.candidateId);
    for (let i = 0; i < this.props.candidate.length; i++) {
      if (this.props.candidate[i].id === this.props.candidateId) {
        localStorage.setItem(
          "payment_id",
          this.props.candidate[i].razorpay_payment_id
        );
      }
    }

    switch (this.props.resumeTestPage) {
      case 0:
        return window.location.replace("/payment");
      case 1:
        return window.location.replace("/instruction/nc");
      case 2:
        localStorage.setItem("savedStateNC", this.getLocalStorageData());
        return window.location.replace("/test/nc");
      case 3:
        return window.location.replace("/instruction/ac");
      case 4:
        localStorage.setItem("savedStateAC", this.getLocalStorageData());
        return window.location.replace("/test/ac");
      case 5:
        return window.location.replace("/instruction/tds");
      case 6:
        localStorage.setItem("savedStateTDS", this.getLocalStorageData());
        return window.location.replace("/test/tds");
      case 7:
        return window.location.replace("/instruction/ve");
      case 8:
        localStorage.setItem("savedStateVE", this.getLocalStorageData());
        return window.location.replace("/test/ve");
      case 9:
        return window.location.replace("/instruction/tm");
      case 10:
        localStorage.setItem("savedStateTM", this.getLocalStorageData());
        return window.location.replace("/test/tm");
      case 11:
        return window.location.replace("/instruction/ar");
      case 12:
        localStorage.setItem("savedStateAR", this.getLocalStorageData());
        return window.location.replace("/test/ar");
      case 13:
        return window.location.replace("/instruction/fm");
      case 14:
        localStorage.setItem("savedStateFM", this.getLocalStorageData());
        return window.location.replace("/test/fm");
      case 15:
        return window.location.replace("/instruction/nvr");
      case 16:
        localStorage.setItem("savedStateNVR", this.getLocalStorageData());
        return window.location.replace("/test/nvr");
      case 17:
        return window.location.replace("/instruction/verbal");
      case 18:
        return window.location.replace("/instruction/vm");
      case 19:
        localStorage.setItem("savedStateVM", this.getLocalStorageData());
        return window.location.replace("/test/vm");
      case 20:
        return window.location.replace("/instruction/vh");
      case 21:
        localStorage.setItem("savedStateVH", this.getLocalStorageData());
        return window.location.replace("/test/vh");
      // case 22:
      //   return window.location.replace("/instruction/vk");
      // case 23:
      //   localStorage.setItem("savedStateVK", this.getLocalStorageData());
      //   return window.location.replace("/test/vk");
      case 24:
        return window.location.replace("/instruction/analogy");
      case 25:
        localStorage.setItem("savedStateAnalogy", this.getLocalStorageData());
        return window.location.replace("/test/analogy");
      case 26:
        return window.location.replace("/instruction/reflection");
      case 27:
        localStorage.setItem(
          "savedStateReflection",
          this.getLocalStorageData()
        );
        return window.location.replace("/test/reflection");
      case 28:
        return window.location.replace("/instruction/series");
      case 29:
        localStorage.setItem("savedStateSeries", this.getLocalStorageData());
        return window.location.replace("/test/series");
      case 30:
        return window.location.replace("/instruction/classification");
      case 31:
        localStorage.setItem(
          "savedStateClassification",
          this.getLocalStorageData()
        );
        return window.location.replace("/test/classification");
      case 32:
        return window.location.replace("/instruction/interest");
      case 33:
        localStorage.setItem("savedStateInterest", this.getLocalStorageData());
        return window.location.replace("/test/interest");
      case 34:
        return window.location.replace("/instruction/adjustment");
      case 35:
        localStorage.setItem(
          "savedStateAdjustment",
          this.getLocalStorageData()
        );
        return window.location.replace("/test/adjustment");
      case 36:
        return window.location.replace("/result");
      default:
        console.log("default");
      // return window.location.replace("/instruction/nc");
    }
  };

  messageClosed = () => {
    this.props.clearWrongResumeCredential();
  };

  componentDidMount() {
    this.props.getCandidateInfo();
  }

  componentDidUpdate() {
    if (this.props.isResumeTest === true) {
      message.success("Successfully resumed test", 2);
      this.redirectToContinueTest();
    } else if (this.props.isResumeTest === false) {
      message.error("Wrong Name or Email", 3, this.messageClosed());
    }
  }
  render() {
    return (
      <Card style={{ width: 500, left: 450, top: 150, position: "absolute" }}>
        <p>
          Give the exact full name and email address you have used first time
          while giving your test. If you use different name and email address
          the test will not resume.
        </p>
        <Form onFinish={this.onFinish}>
          <Form.Item
            label="Name"
            name="name"
            rules={[{ required: true, message: "Please input your name!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: "Please input your Email Address!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Resume
            </Button>
          </Form.Item>
        </Form>
      </Card>
    );
  }
}

const mapStateToProps = (state) => ({
  isResumeTest: state.candidateReducer.isResumeTest,
  resumeTestData: state.candidateReducer.resumeTestData,
  resumeTestPage: state.candidateReducer.resumeTestPage,
  candidate: state.candidateReducer.candidate,
  candidateId: state.candidateReducer.candidateId,
});

const mapDispatchToProps = (dispatch) => {
  return {
    resumeTest: (params) => dispatch(resumeTest(params)),
    clearWrongResumeCredential: () => dispatch(clearWrongResumeCredential()),
    getCandidateInfo: () => dispatch(getCandidateInfo()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ResumeTest);
