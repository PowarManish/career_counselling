import { Button, Card } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import { getCandidateInfo } from "../../../actions/candidateActions";
import * as localStorage from "../../../utility/localStorage";
import axios from "axios";

export class Payment extends Component {
  loadScript = (src) => {
    return new Promise((resolve) => {
      const script = document.createElement("script");
      script.src = src;
      script.onload = () => {
        resolve(true);
      };
      script.onerror = () => {
        resolve(false);
      };
      document.body.appendChild(script);
    });
  };
  getCandidateData = () => {
    for (let i = 0; i < this.props.candidate.length; i++) {
      if (localStorage.getItem("candidate_id") === this.props.candidate[i].id) {
        return this.props.candidate[i];
      }
    }
  };

  displayRazorpay = async () => {
    const res = await this.loadScript(
      "https://checkout.razorpay.com/v1/checkout.js"
    );

    if (!res) {
      alert("Razorpay SDK failed to load. Are you online?");
      return;
    }

    const data = await fetch(
      "https://apicareerdishaforyou.tk/api/razorpay/createOrder",
      {
        method: "POST",
        body: JSON.stringify({
          candidate_id: localStorage.getItem("candidate_id"),
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      }
    ).then((t) => t.json());

    const candidateData = this.getCandidateData();
    var options = {
      key: "rzp_live_agbLuznJRx4noY",
      currency: data.currency,
      amount: data.amount.toString(),
      order_id: data.id,
      name: "Career Disha For You",
      description: "Fee",
      handler: async function (response) {
        var successData = {
          order_id: data.id,
          razorpay_payment_id: response.razorpay_payment_id,
          razorpay_order_id: response.razorpay_order_id,
          razorpay_signature: response.razorpay_signature,
          candidate_id: localStorage.getItem("candidate_id"),
        };
        const result = await axios.post(
          "https://apicareerdishaforyou.tk/api/razorpay/success",
          successData
        );

        if (result.data.isLegit === true) {
          localStorage.setItem("payment_id", response.razorpay_payment_id);
          window.location.href = "/instruction/nc";
        }
      },
      prefill: {
        name: candidateData.name,
        email: candidateData.email,
        contact: candidateData.phone_number,
      },
      notes: {
        address: "Razorpay Corporate Office",
      },
      theme: {
        color: "#F37254",
      },
    };
    var paymentObject = new window.Razorpay(options);

    paymentObject.open();
  };

  componentDidMount() {
    this.props.getCandidateInfo();
  }

  render() {
    return (
      <div>
        <Card style={{ marginLeft: 450, width: 300 }}>
          <p>
            Payment is Rs 1000. By clicking below button you could do payment.
            We support Credit Card ,Debit Card,UPI,Netbanking. Once done payment
            can not be refunded.
          </p>
          <Button type="primary" onClick={this.displayRazorpay}>
            Click Here
          </Button>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  candidate: state.candidateReducer.candidate,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getCandidateInfo: () => dispatch(getCandidateInfo()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);
