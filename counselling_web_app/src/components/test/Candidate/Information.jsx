import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Form,
  Button,
  Input,
  DatePicker,
  Divider,
  Select,
  Card,
  Row,
  Col,
  Typography,
  InputNumber,
  message,
} from "antd";

import { postCandidateInfo } from "../../../actions/candidateActions";

import moment from "moment";

const { TextArea } = Input;
const { Option } = Select;
// const layout = {
//   labelCol: { span: 8 },
//   wrapperCol: { span: 8 },
// };
const firstItemLayout = {
  labelCol: {
    offset: 0,
    span: 8,
  },
  wrapperCol: {
    span: 8,
  },
};
const secondItemLayout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 8,
  },
};
const rowItemLayout = {
  labelCol: {
    offset: 0,
    span: 8,
  },
  wrapperCol: {
    span: 12,
  },
};
const STREAMS = ["Engineering", "Medical", "Commerce", "Arts", "Fine Arts"];
const { Title } = Typography;
const getFutureDatesDisabled = (current) => {
  return !(current && current.valueOf() < Date.now());
};
export class Information extends Component {
  state = {
    selectedStreams: 0,
  };

  handleSelectAll = (value) => {
    console.log(value.length);
    this.setState({ selectedStreams: value.length });
  };
  onFinish = (values) => {
    if (this.state.selectedStreams !== 5) {
      message.error("Please select all streams", 3);
    } else {
      values.dob = moment.utc(values.dateOfBirth).local().format("YYYY/MM/DD");
      this.props.postCandidateInfo(values);
    }
  };

  componentDidUpdate() {
    if (this.props.candidateId !== 0) {
      localStorage.setItem("candidate_id", this.props.candidateId);
      return this.props.history.replace(`/payment`);
    }
  }
  render() {
    return (
      <Card title="Please fill your information...">
        <Form onFinish={this.onFinish} scrollToFirstError>
          <Row>
            <Col span={12}>
              <Form.Item
                label="Name"
                name="name"
                rules={[{ required: true, message: "Please input your name!" }]}
                {...firstItemLayout}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              {/* <Form.Item label="Date" name="date">
            <DatePicker
              defaultValue={moment(moment().local(), "DD/MM/YYYY")}
              format={"DD/MM/YYYY"}
              disabled={true}
            />
          </Form.Item> */}
              <Form.Item
                label="School Name"
                name="school_name"
                rules={[
                  { required: true, message: "Please input your School Name!" },
                ]}
                {...secondItemLayout}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Email"
                name="email"
                rules={[
                  { required: true, message: "Please input your email!" },
                ]}
                {...firstItemLayout}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Phone Number"
                name="phone_number"
                rules={[
                  {
                    required: true,
                    message: "Please input your phone number!",
                  },
                ]}
                {...secondItemLayout}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Standard"
                name="standard"
                rules={[{ required: true, message: "Please input standard!" }]}
                {...firstItemLayout}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Date of Birth"
                name="dateOfBirth"
                rules={[
                  {
                    required: true,
                    message: "Please input your Date of Birth!",
                  },
                ]}
                {...secondItemLayout}
              >
                <DatePicker
                  format={"DD/MM/YYYY"}
                  disabledDate={getFutureDatesDisabled}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Age"
                name="age"
                rules={[{ required: true, message: "Please input age!" }]}
                {...firstItemLayout}
              >
                <InputNumber type="number" style={{ width: "100%" }} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Mother Tongue"
                name="motherTongue"
                rules={[
                  { required: true, message: "Please input mother tongue!" },
                ]}
                {...secondItemLayout}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Permanant Address"
                name="permanant_address"
                rules={[
                  { required: true, message: "Please input your address!" },
                ]}
                {...firstItemLayout}
              >
                <TextArea />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Medium Of Schooling"
                name="mediumOfInstruction"
                rules={[
                  {
                    required: true,
                    message: "Please input  medium of school!",
                  },
                ]}
                {...secondItemLayout}
              >
                <Input />
              </Form.Item>
            </Col>
          </Row>
          <Divider style={{ borderColor: "#000000" }} />

          <Title level={4}>Personal Information</Title>

          <Form.Item
            label="Hobby"
            name="hobby"
            rules={[{ required: true, message: "Please input your hobby!" }]}
            {...rowItemLayout}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Types of Books You Read If Any"
            name="typeOfBook"
            rules={[
              {
                required: true,
                message: "Please input type of book you like!",
              },
            ]}
            {...rowItemLayout}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Do You Have Separate Room For Study"
            name="studyRoom"
            rules={[{ required: true, message: "Please input the answer!" }]}
            {...rowItemLayout}
          >
            <Select>
              <Option value={true}>Yes</Option>
              <Option value={false}>No</Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="Do You Go For Private Tutions"
            name="privateTution"
            rules={[{ required: true, message: "Please input the answer!" }]}
            {...rowItemLayout}
          >
            <Select>
              <Option value={true}>Yes</Option>
              <Option value={false}>No</Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="Subjects For Which You Have Private Tutions"
            name="privateTutionSubjects"
            rules={[{ required: true, message: "Please input the answer!" }]}
            {...rowItemLayout}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Choose The Following Streams In Your Preferred Order"
            name="preferredStreams"
            rules={[{ required: true, message: "Please input the answer!" }]}
            {...rowItemLayout}
          >
            <Select
              mode="multiple"
              name="streamPreferance"
              showSearch
              onChange={this.handleSelectAll}
            >
              {STREAMS.map((item) => (
                <Option key={item} value={item}>
                  {item}
                </Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            label="Favourite Subject"
            name="favouriteSubject"
            rules={[
              {
                required: true,
                message: "Please input your Favourite Subject !",
              },
            ]}
            {...rowItemLayout}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Disliked Subject"
            name="dislikedSubject"
            rules={[
              {
                required: true,
                message: "Please input your Disliked subject !",
              },
            ]}
            {...rowItemLayout}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="What Curriculam You Should Take(Parents Choice)"
            name="parentsChoice"
            rules={[{ required: true, message: "Please input your answer !" }]}
            {...rowItemLayout}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="What Curriculam You Should Take (Your Choice)"
            name="candidatesChoice"
            rules={[{ required: true, message: "Please input your answer !" }]}
            {...rowItemLayout}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Card>
    );
  }
}

const mapStateToProps = (state) => ({
  candidateId: state.candidateReducer.candidateId,
});

const mapDispatchToProps = (dispatch) => {
  return {
    postCandidateInfo: (values) => dispatch(postCandidateInfo(values)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Information);
