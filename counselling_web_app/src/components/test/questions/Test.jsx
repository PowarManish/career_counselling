import React, { Component } from "react";
import { Typography, Card, Button, Radio, Space, Row } from "antd";
import { injectIntl, FormattedMessage } from "react-intl";
import Timer from "../../common/Timer";
import { questions } from "../../../locale/en.json";
import { connect } from "react-redux";
import {
  getOptionsByQustion,
  getTestIdFromTestName,
} from "../../../utility/generalFunctions";
import {
  saveAnswer,
  updateAnswer,
  fetchTest,
  submitTest,
  clearSubmitTest,
} from "../../../actions/testActions";
import Answers from "../../../utility/Answers.json";
import * as localStorage from "../../../utility/localStorage";
const { Paragraph } = Typography;

export class Test extends Component {
  state = {
    page: 1,
    selectedAns: [],
    remainingSeconds: this.props.testTime,
  };

  previous = () => {
    var page = this.state.page;
    page--;
    this.setState({ page });
    localStorage.setItem("savedState" + this.props.testName, this.state);
    window.scrollTo(0, 0);
  };

  next = () => {
    var page = this.state.page;
    page++;
    this.setState({ page });
    localStorage.setItem("savedState" + this.props.testName, this.state);
    window.scrollTo(0, 0);
  };

  search = (nameKey, myArray) => {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].questionNo === nameKey) {
        return myArray[i];
      }
    }
  };

  onChange = (e) => {
    var selectedAns = this.state.selectedAns;
    var values = {
      testId: getTestIdFromTestName(this.props.test, this.props.testName),
      candidateId: localStorage.getItem("candidate_id"),
      questionNumber: e.target.name + 1,
      answer: e.target.value,
      correctAnswer: Answers[this.props.testName][e.target.name],
      remainingSeconds: this.state.remainingSeconds,
    };
    var isAlreadyAnswered = this.search(e.target.name + 1, selectedAns);
    if (isAlreadyAnswered === undefined) {
      selectedAns.push({
        questionNo: e.target.name + 1,
        answer: e.target.value,
        remainingSeconds: this.state.remainingSeconds,
      });
      this.props.saveAnswer(values);
    } else {
      for (var i = 0; i < selectedAns.length; i++) {
        if (selectedAns[i].questionNo === e.target.name + 1) {
          selectedAns[i].answer = e.target.value;
          this.props.updateAnswer(values);
        }
      }
    }
    this.setState({ selectedAns });
    localStorage.setItem("savedState" + this.props.testName, this.state);
  };

  getVal = (i) => {
    var resultObject = this.search(i, this.state.selectedAns);
    if (resultObject === undefined) {
      return;
    }
    return resultObject.answer;
  };

  nextButtonDisabled = () => {
    if (this.state.page >= Math.ceil(questions[this.props.testName].length / 5))
      return true;
    else if (
      this.props.testName === "NC" ||
      this.props.testName === "Interest" ||
      this.props.testName === "Adjustment"
    ) {
      var questionsOnPage = [];
      var j = this.state.page * 5;
      for (let i = j - 4; i <= j; i++) {
        questionsOnPage.push(i);
      }
      var ansGivenArr = [];
      for (let i = 0; i < this.state.selectedAns.length; i++) {
        ansGivenArr.push(this.state.selectedAns[i].questionNo);
      }
      const result = questionsOnPage.every((val) => ansGivenArr.includes(val));
      return !result;
    } else return false;
  };

  submit = () => {
    var values = {
      candidateId: localStorage.getItem("candidate_id"),
      testId: getTestIdFromTestName(this.props.test, this.props.testName),
      testName: this.props.testName,
    };
    this.props.submitTest(values);
  };

  componentDidMount() {
    this.props.fetchTest();
  }

  componentDidUpdate() {
    if (this.props.isTestSubmitted) {
      localStorage.removeItem("savedState" + this.props.testName);
      this.props.clearSubmitTest();
      this.props.submit();
    }
  }
  componentWillMount() {
    var savedState = localStorage.getItem("savedState" + this.props.testName);
    if (savedState != null) {
      this.setState({
        page: savedState.page,
        selectedAns: savedState.selectedAns,
        remainingSeconds: savedState.remainingSeconds,
      });
    }
  }

  SetTimerRemainingSeconds = (remainingSeconds) => {
    this.setState({ remainingSeconds });
    localStorage.setItem("savedState" + this.props.testName, this.state);
  };

  render() {
    let menuItems = [];
    var j = this.state.page * 5 - 5;
    for (var i = j; i < j + 5; i++) {
      if (
        this.props.intl.messages[
          `questions.${this.props.testName + "." + i}.question`
        ]
      ) {
        menuItems.push(
          <Card
            style={{
              width: 800,
            }}
          >
            <Paragraph strong={true}>
              {"Q." + (i + 1) + " "}
              {
                this.props.intl.messages[
                  `questions.${this.props.testName + "." + i}.question`
                ]
              }
            </Paragraph>
            <Radio.Group
              value={this.getVal(i + 1)}
              key={i}
              name={i}
              onChange={this.onChange}
            >
              {getOptionsByQustion(
                this.props.testName,
                i,
                this.props.intl.messages
              )}
            </Radio.Group>
          </Card>
        );
      }
    }

    return (
      <React.Fragment>
        {this.props.testTime && (
          <Timer
            remainingSeconds={this.state.remainingSeconds}
            finish={this.submit}
            setRemainingSeconds={this.SetTimerRemainingSeconds}
          />
        )}
        <Space
          direction="vertical"
          className="center"
          style={{ marginTop: 30 }}
        >
          {menuItems}
          <Row>
            <Button
              style={{ left: 272, position: "absolute" }}
              type="primary"
              disabled={this.state.page === 1 ? true : false}
              onClick={this.previous}
            >
              <FormattedMessage id="common.previous" />
            </Button>

            <Button
              type="primary"
              style={{ right: 272, position: "absolute" }}
              disabled={this.nextButtonDisabled()}
              onClick={this.next}
            >
              <FormattedMessage id="common.next" />
            </Button>
            <Button
              type="primary"
              disabled={
                !(this.state.page >=
                Math.ceil(questions[this.props.testName].length / 5)
                  ? true
                  : false)
              }
              onClick={this.submit}
              style={{ position: "" }}
            >
              <FormattedMessage id="common.submit" />
            </Button>
          </Row>
        </Space>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  test: state.testReducer.test,
  isTestSubmitted: state.testReducer.isTestSubmitted,
});

const mapDispatchToProps = (dispatch) => {
  return {
    saveAnswer: (values) => dispatch(saveAnswer(values)),
    updateAnswer: (values) => dispatch(updateAnswer(values)),
    fetchTest: (values) => dispatch(fetchTest(values)),
    submitTest: (values) => dispatch(submitTest(values)),
    clearSubmitTest: () => dispatch(clearSubmitTest()),
  };
};

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Test));
