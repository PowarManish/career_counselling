import React, { Component } from "react";
import DiagramTest from "./DiagramTest";
import {
  savePageNumber,
  clearSavePageNumber,
} from "../../../actions/candidateActions";
import { connect } from "react-redux";

export class TDS extends Component {
  submit = () => {
    const data = {
      pageNumber: 7,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/instruction/ve");
    }
  }

  render() {
    return (
      <div>
        <DiagramTest
          numberOfQuestions={40}
          testTime={360}
          testName="TDS"
          numberOfOptions={4}
          submit={this.submit}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TDS);
