import React, { Component } from "react";
import { Card, Button, Radio, Space, Row } from "antd";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import {
  saveAnswer,
  updateAnswer,
  fetchTest,
  submitTest,
  clearSubmitTest,
} from "../../../actions/testActions";
import { getTestIdFromTestName } from "../../../utility/generalFunctions";
import Timer from "../../common/Timer";
import Answers from "../../../utility/Answers.json";
import * as localStorage from "../../../utility/localStorage";
export class DiagramTest extends Component {
  state = {
    page: 1,
    selectedAns: [],
    remainingSeconds: this.props.testTime,
  };

  previous = () => {
    var page = this.state.page;
    page--;
    this.setState({ page });
    localStorage.setItem("savedState" + this.props.testName, this.state);
    window.scrollTo(0, 0);
  };

  next = () => {
    var page = this.state.page;
    page++;
    this.setState({ page });
    localStorage.setItem("savedState" + this.props.testName, this.state);
    window.scrollTo(0, 0);
  };

  onChange = (e) => {
    var selectedAns = this.state.selectedAns;
    var values = {
      testId: getTestIdFromTestName(this.props.test, this.props.testName),
      candidateId: localStorage.getItem("candidate_id"),
      questionNumber: e.target.name + 1,
      answer: e.target.value,
      correctAnswer: Answers[this.props.testName][e.target.name],
      remainingSeconds: this.state.remainingSeconds,
    };

    var isAlreadyAnswered = this.search(e.target.name + 1, selectedAns);
    if (isAlreadyAnswered === undefined) {
      selectedAns.push({
        questionNo: e.target.name + 1,
        answer: e.target.value,
        remainingSeconds: this.state.remainingSeconds,
      });
      this.props.saveAnswer(values);
    } else {
      for (var i = 0; i < selectedAns.length; i++) {
        if (selectedAns[i].questionNo === e.target.name + 1) {
          selectedAns[i].answer = e.target.value;
          this.props.updateAnswer(values);
        }
      }
    }
    this.setState({ selectedAns });
    localStorage.setItem("savedState" + this.props.testName, this.state);
  };

  getRadioButtons = () => {
    const radio = [];
    var char = ["A", "B", "C", "D", "E", "F"];
    for (var i = 0; i < this.props.numberOfOptions; i++) {
      radio.push(
        <Radio key={char[i]} value={char[i]}>
          <b>{char[i]}</b>
        </Radio>
      );
    }
    return <React.Fragment>{radio}</React.Fragment>;
  };

  search = (nameKey, myArray) => {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].questionNo === nameKey) {
        return myArray[i];
      }
    }
  };

  getVal = (i) => {
    var resultObject = this.search(i, this.state.selectedAns);
    if (resultObject === undefined) {
      return;
    }
    return resultObject.answer;
  };

  componentDidMount() {
    this.props.fetchTest();
  }

  submit = () => {
    var values = {
      candidateId: localStorage.getItem("candidate_id"),
      testId: getTestIdFromTestName(this.props.test, this.props.testName),
      testName: this.props.testName,
    };
    this.props.submitTest(values);
  };

  SetTimerRemainingSeconds = (remainingSeconds) => {
    this.setState({ remainingSeconds });
    localStorage.setItem("savedState" + this.props.testName, this.state);
  };

  componentDidUpdate() {
    if (this.props.isTestSubmitted) {
      localStorage.removeItem("savedState" + this.props.testName);
      this.props.clearSubmitTest();
      this.props.submit();
    }
  }

  componentWillMount() {
    var savedState = localStorage.getItem("savedState" + this.props.testName);
    if (savedState != null) {
      this.setState({
        page: savedState.page,
        selectedAns: savedState.selectedAns,
        remainingSeconds: savedState.remainingSeconds,
      });
    }
  }

  render() {
    let menuItems = [];
    let j = this.state.page * 5 - 5;

    if (this.props.numberOfQuestions % 5 === 0) {
      if (this.state.page <= Math.ceil(this.props.numberOfQuestions / 5)) {
        for (let i = j; i < j + 5; i++) {
          menuItems.push(
            <Card key={i}>
              <b>{"Q." + (i + 1) + " "}</b>
              <img
                src={require(`../../../images/${this.props.testName}/${
                  i + 1
                }.png`)}
                style={{ marginTop: 10, marginBottom: 10 }}
                alt="not found"
              />
              <Row>
                <Radio.Group
                  value={this.getVal(i + 1)}
                  key={i}
                  name={i}
                  onChange={this.onChange}
                  style={{ marginLeft: 20, marginTop: 5 }}
                >
                  {this.getRadioButtons()}
                </Radio.Group>
              </Row>
            </Card>
          );
        }
      }
    } else {
      if (this.state.page < Math.ceil(this.props.numberOfQuestions / 5)) {
        for (let i = j; i < j + 5; i++) {
          menuItems.push(
            <Card
              style={{
                width: 800,
              }}
            >
              {"Q." + (i + 1) + " "}
              <img
                src={require(`../../../images/${this.props.testName}/${
                  i + 1
                }.png`)}
                style={{ marginTop: 10, marginBottom: 10 }}
                alt="not found"
              />
              <Row>
                <Radio.Group
                  value={this.getVal(i + 1)}
                  key={i}
                  name={i}
                  onChange={this.onChange}
                  style={{ marginLeft: 20, marginTop: 5 }}
                >
                  {this.getRadioButtons()}
                </Radio.Group>
              </Row>
            </Card>
          );
        }
      } else {
        for (let i = j; i < j + (this.props.numberOfQuestions % 5); i++) {
          menuItems.push(
            <Card
              style={{
                width: 800,
              }}
            >
              {"Q." + (i + 1) + " "}
              <img
                src={require(`../../../images/${this.props.testName}/${
                  i + 1
                }.png`)}
                style={{ marginTop: 10, marginBottom: 10 }}
                alt="not found"
              />
              <Row>
                <Radio.Group
                  value={this.getVal(i + 1)}
                  key={i}
                  name={i}
                  onChange={this.onChange}
                  style={{ marginLeft: 20, marginTop: 5 }}
                >
                  {this.getRadioButtons()}
                </Radio.Group>
              </Row>
            </Card>
          );
        }
      }
    }
    return (
      <div>
        <Timer
          remainingSeconds={this.state.remainingSeconds}
          finish={this.submit}
          setRemainingSeconds={this.SetTimerRemainingSeconds}
        />
        <Space
          direction="vertical"
          className="center"
          style={{ marginTop: 30 }}
        >
          {menuItems}
          <Row>
            <Button
              style={{ left: 272, position: "absolute" }}
              type="primary"
              disabled={this.state.page === 1 ? true : false}
              onClick={this.previous}
            >
              <FormattedMessage id="common.previous" />
            </Button>

            <Button
              type="primary"
              style={{ right: 272, position: "absolute" }}
              disabled={
                this.state.page >= Math.ceil(this.props.numberOfQuestions / 5)
                  ? true
                  : false
              }
              onClick={this.next}
            >
              <FormattedMessage id="common.next" />
            </Button>
            <Button
              type="primary"
              disabled={
                !(this.state.page >= Math.ceil(this.props.numberOfQuestions / 5)
                  ? true
                  : false)
              }
              onClick={this.submit}
              style={{ position: "" }}
            >
              <FormattedMessage id="common.submit" />
            </Button>
          </Row>
        </Space>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  test: state.testReducer.test,
  isTestSubmitted: state.testReducer.isTestSubmitted,
});

const mapDispatchToProps = (dispatch) => {
  return {
    saveAnswer: (values) => dispatch(saveAnswer(values)),
    updateAnswer: (values) => dispatch(updateAnswer(values)),
    fetchTest: (values) => dispatch(fetchTest(values)),
    submitTest: (values) => dispatch(submitTest(values)),
    clearSubmitTest: () => dispatch(clearSubmitTest()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DiagramTest);
