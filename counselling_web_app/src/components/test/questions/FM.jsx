import React, { Component } from "react";
import { Card, Button, Radio, Row } from "antd";
import SplitPane from "react-split-pane";
import {
  getOptionsByQustion,
  getTestIdFromTestName,
} from "../../../utility/generalFunctions";
import { connect } from "react-redux";
import { injectIntl, FormattedMessage } from "react-intl";
import Timer from "../../common/Timer";
import {
  saveAnswer,
  updateAnswer,
  fetchTest,
  submitTest,
  clearSubmitTest,
} from "../../../actions/testActions";
import Answers from "../../../utility/Answers.json";
import * as localStorage from "../../../utility/localStorage";
import {
  savePageNumber,
  clearSavePageNumber,
} from "../../../actions/candidateActions";

export class FM extends Component {
  state = {
    page: 1,
    selectedAns: [],
    remainingSeconds: 360,
  };

  search = (nameKey, myArray) => {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].questionNo === nameKey) {
        return myArray[i];
      }
    }
  };

  submit = () => {
    var values = {
      candidateId: localStorage.getItem("candidate_id"),
      testId: getTestIdFromTestName(this.props.test, "FM"),
      testName: "FM",
    };
    this.props.submitTest(values);
  };

  previous = () => {
    var page = this.state.page;
    page--;
    this.setState({ page });
    localStorage.setItem("savedStateFM", this.state);
    window.scrollTo(0, 0);
  };

  next = () => {
    var page = this.state.page;
    page++;
    this.setState({ page });
    localStorage.setItem("savedStateFM", this.state);
    window.scrollTo(0, 0);
  };

  getVal = (i) => {
    var resultObject = this.search(i, this.state.selectedAns);
    if (resultObject === undefined) {
      return;
    }
    return resultObject.answer;
  };

  componentDidMount() {
    this.props.fetchTest();
  }

  componentDidUpdate() {
    if (this.props.isTestSubmitted) {
      localStorage.removeItem("savedStateFM");
      this.props.clearSubmitTest();
      const data = {
        pageNumber: 15,
        candidateId: localStorage.getItem("candidate_id"),
      };
      this.props.savePageNumber(data);
    } else if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/instruction/nvr");
    }
  }

  onChange = (e) => {
    var selectedAns = this.state.selectedAns;
    var values = {
      testId: getTestIdFromTestName(this.props.test, "FM"),
      candidateId: localStorage.getItem("candidate_id"),
      questionNumber: e.target.name + 1,
      answer: e.target.value,
      correctAnswer: Answers["FM"][e.target.name],
      remainingSeconds: this.state.remainingSeconds,
    };

    var isAlreadyAnswered = this.search(e.target.name + 1, selectedAns);
    if (isAlreadyAnswered === undefined) {
      selectedAns.push({
        questionNo: e.target.name + 1,
        answer: e.target.value,
        remainingSeconds: this.state.remainingSeconds,
      });
      this.props.saveAnswer(values);
    } else {
      for (var i = 0; i < selectedAns.length; i++) {
        if (selectedAns[i].questionNo === e.target.name + 1) {
          selectedAns[i].answer = e.target.value;
          this.props.updateAnswer(values);
        }
      }
    }
    this.setState({ selectedAns });
    localStorage.setItem("savedStateFM", this.state);
  };

  SetTimerRemainingSeconds = (remainingSeconds) => {
    this.setState({ remainingSeconds });
    localStorage.setItem("savedStateFM", this.state);
  };

  componentWillMount() {
    var savedState = localStorage.getItem("savedStateFM");
    if (savedState != null) {
      this.setState({
        page: savedState.page,
        selectedAns: savedState.selectedAns,
        remainingSeconds: savedState.remainingSeconds,
      });
    }
  }

  render() {
    let menuItems = [];
    var j = this.state.page * 5 - 5;

    for (var i = j; i < j + 5; i++) {
      menuItems.push(
        <Card style={{ marginTop: 10, marginBottom: 10 }}>
          <b>{"Q." + (i + 1) + " "}</b>
          <Radio.Group
            value={this.getVal(i + 1)}
            key={i}
            name={i}
            onChange={this.onChange}
          >
            {getOptionsByQustion("FM", i, this.props.intl.messages)}
          </Radio.Group>
        </Card>
      );
    }

    return (
      <div>
        <Timer
          remainingSeconds={this.state.remainingSeconds}
          finish={this.submit}
          setRemainingSeconds={this.SetTimerRemainingSeconds}
        />
        <SplitPane
          split="vertical"
          minSize={500}
          style={{ marginLeft: 50, marginRight: 50 }}
        >
          <div>
            {this.state.page > 5 ? (
              <Card>
                <Row>
                  <img
                    src={require("../../../images/FM/3.png")}
                    alt="not found"
                    style={{ marginBottom: 10 }}
                  />
                </Row>
                <Row>
                  <img
                    src={require("../../../images/FM/4.png")}
                    alt="not found"
                  />
                </Row>
              </Card>
            ) : (
              <Card>
                <Row>
                  <img
                    src={require("../../../images/FM/1.png")}
                    alt="not found"
                    style={{ marginBottom: 10 }}
                  />
                </Row>
                <Row>
                  <img
                    src={require("../../../images/FM/2.png")}
                    alt="not found"
                  />
                </Row>
              </Card>
            )}
          </div>
          <div style={{ marginLeft: 20 }}>
            {menuItems}
            <Row>
              <Button
                style={{ left: 20, position: "absolute" }}
                type="primary"
                disabled={this.state.page === 1 ? true : false}
                onClick={this.previous}
              >
                <FormattedMessage id="common.previous" />
              </Button>

              <Button
                type="primary"
                style={{ right: 0, position: "absolute" }}
                disabled={this.state.page === 12 ? true : false}
                onClick={this.next}
              >
                <FormattedMessage id="common.next" />
              </Button>
              <Button
                type="primary"
                disabled={this.state.page !== 12}
                onClick={this.submit}
                style={{ left: 300, position: "absolute" }}
              >
                <FormattedMessage id="common.submit" />
              </Button>
            </Row>
          </div>
        </SplitPane>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  test: state.testReducer.test,
  isTestSubmitted: state.testReducer.isTestSubmitted,
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});

const mapDispatchToProps = (dispatch) => {
  return {
    saveAnswer: (values) => dispatch(saveAnswer(values)),
    updateAnswer: (values) => dispatch(updateAnswer(values)),
    fetchTest: (values) => dispatch(fetchTest(values)),
    submitTest: (values) => dispatch(submitTest(values)),
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
    clearSubmitTest: () => dispatch(clearSubmitTest()),
  };
};

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(FM));
