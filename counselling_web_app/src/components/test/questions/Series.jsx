import React, { Component } from "react";
import DiagramTest from "./DiagramTest";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../actions/candidateActions";
import { connect } from "react-redux";

export class Series extends Component {
  submit = () => {
    const data = {
      pageNumber: 30,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/instruction/classification");
    }
  }

  render() {
    return (
      <div>
        <DiagramTest
          numberOfQuestions={20}
          testTime={240}
          testName="Series"
          numberOfOptions={5}
          submit={this.submit}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});

const mapDispatchToProps = (dispatch) => {
  return {
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
    savePageNumber: (values) => dispatch(savePageNumber(values)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Series);
