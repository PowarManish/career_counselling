import React, { Component } from "react";
import Test from "./Test";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../actions/candidateActions";
import { connect } from "react-redux";

export class AC extends Component {
  submit = () => {
    const data = {
      pageNumber: 5,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/instruction/tds");
    }
  }

  render() {
    return (
      <div>
        <Test testName="AC" testTime={360} submit={this.submit} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});

const mapDispatchToProps = (dispatch) => {
  return {
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
    savePageNumber: (values) => dispatch(savePageNumber(values)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AC);
