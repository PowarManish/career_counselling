import React, { Component } from "react";
import { Typography, Card, Modal, Button, Row } from "antd";
import { FormattedMessage } from "react-intl";
const { Text, Title, Paragraph } = Typography;

export class Instruction extends Component {
  state = { visible: false, startTestModal: false };

  showVideo = () => {
    this.setState({ visible: true });
  };
  closeModal = () => {
    this.setState({
      visible: false,
    });
  };
  openStartTestModal = () => {
    this.setState({ startTestModal: true });
  };

  closeStartTestModal = () => {
    this.setState({ startTestModal: false });
  };
  render() {
    return (
      <div>
        <Card style={{ marginLeft: 100, marginRight: 100, marginTop: 25 }}>
          <Typography>
            <Title level={2} align="center">
              <FormattedMessage id={this.props.testName} />
            </Title>
            <Paragraph>
              <FormattedMessage id={this.props.instructionPart1} />
            </Paragraph>
            <Text strong>
              <FormattedMessage id="common.text1" />
            </Text>

            <Paragraph>
              <FormattedMessage
                id={this.props.questions}
                values={{
                  linebreak: <br />,
                }}
              />
            </Paragraph>

            <Text>
              <FormattedMessage id="common.text2" />
            </Text>
            <Row>
              <Button
                onClick={this.showVideo}
                type="primary"
                style={{ margin: 10 }}
              >
                See Video
              </Button>
            </Row>

            <Paragraph>
              <FormattedMessage id={this.props.instructionPart2} />
            </Paragraph>
            <Button
              type="primary"
              style={{ margin: 10 }}
              onClick={this.openStartTestModal}
            >
              Start
            </Button>
          </Typography>
        </Card>
        <Modal
          title="Instructions"
          visible={this.state.visible}
          onCancel={this.closeModal}
          onOk={this.closeModal}
          destroyOnClose={true}
        >
          <iframe
            title="Title"
            src={this.props.videoUrl}
            frameborder="10"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture;fullscreen"
          ></iframe>
        </Modal>
        <Modal
          title="Start Test"
          visible={this.state.startTestModal}
          onCancel={this.closeStartTestModal}
          destroyOnClose={true}
          footer={[
            <Button key="back" onClick={this.closeStartTestModal}>
              Close
            </Button>,
          ]}
        >
          <Card>
            <p>
              Read instructions carefully before starting test.Once test started
              you can not go back to read instruction. If you use browser back
              button test will be terminated.
            </p>
            <Button type="primary" onClick={this.props.startTest}>
              Start Test
            </Button>
          </Card>
        </Modal>
      </div>
    );
  }
}

export default Instruction;
