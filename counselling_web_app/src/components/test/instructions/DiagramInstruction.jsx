import React, { Component } from "react";
import { Typography, Card, Modal, Button, Row } from "antd";
import { FormattedMessage } from "react-intl";

const { Text, Title, Paragraph } = Typography;
export class DiagramInstruction extends Component {
  state = { visible: false };

  showVideo = () => {
    this.setState({ visible: true, startTestModal: false });
  };
  closeModal = () => {
    this.setState({
      visible: false,
    });
  };
  // changeLanguage = () => {
  //   var { isMarathi } = this.state;
  //   this.setState({ isMarathi: !isMarathi });
  // };
  openStartTestModal = () => {
    this.setState({ startTestModal: true });
  };

  closeStartTestModal = () => {
    this.setState({ startTestModal: false });
  };
  render() {
    return (
      <div>
        <Card style={{ marginLeft: 100, marginRight: 100, marginTop: 25 }}>
          {/* <Button onClick={this.changeLanguage}>English</Button> */}
          <Typography>
            <Title level={2} align="center">
              <FormattedMessage id={this.props.testName} />
            </Title>

            <Paragraph>
              <FormattedMessage id={this.props.instructionPart1} />
            </Paragraph>
            <Text strong>
              <FormattedMessage id="common.text1" />
            </Text>
            {this.props.image1 !== undefined && (
              <Row>
                <img
                  src={this.props.image1}
                  style={{ marginTop: 10, marginBottom: 10 }}
                  alt=""
                />
              </Row>
            )}
            {this.props.question1 !== undefined && (
              <Paragraph>
                <FormattedMessage id={this.props.question1} />
              </Paragraph>
            )}
            {this.props.image2 !== undefined && (
              <Row>
                <img
                  src={this.props.image2}
                  style={{ marginTop: 10, marginBottom: 10 }}
                  alt=""
                />
              </Row>
            )}
            {this.props.question2 !== undefined && (
              <Paragraph>
                <FormattedMessage id={this.props.question2} />
              </Paragraph>
            )}
            {this.props.image3 !== undefined && (
              <Row>
                <img
                  src={this.props.image3}
                  style={{ marginTop: 10, marginBottom: 10 }}
                  alt=""
                />
              </Row>
            )}
            {this.props.question3 !== undefined && (
              <Paragraph>
                <FormattedMessage
                  id={this.props.question3}
                  values={{
                    linebreak: <br />,
                  }}
                />
              </Paragraph>
            )}
            {this.props.image4 !== undefined && (
              <Row>
                <img
                  src={this.props.image4}
                  style={{ marginTop: 10, marginBottom: 10 }}
                  alt=""
                />
              </Row>
            )}
            {this.props.question4 !== undefined && (
              <Paragraph>
                <FormattedMessage id={this.props.question4} />
              </Paragraph>
            )}
            {this.props.image5 !== undefined && (
              <Row>
                <img
                  src={this.props.image5}
                  style={{ marginTop: 10, marginBottom: 10 }}
                  alt=""
                />
              </Row>
            )}
            {this.props.question5 !== undefined && (
              <Paragraph>
                <FormattedMessage id={this.props.question5} />
              </Paragraph>
            )}
            {this.props.image6 !== undefined && (
              <Row>
                <img
                  src={this.props.image6}
                  style={{ marginTop: 10, marginBottom: 10 }}
                  alt=""
                />
              </Row>
            )}
            {this.props.question6 !== undefined && (
              <Paragraph>
                <FormattedMessage id={this.props.question6} />
              </Paragraph>
            )}
            <Text>
              <FormattedMessage id="common.text2" />
            </Text>
            <Row>
              <Button
                onClick={this.showVideo}
                type="primary"
                style={{ margin: 10 }}
              >
                See Video
              </Button>
            </Row>

            <Paragraph>
              <FormattedMessage id={this.props.instructionPart2} />
            </Paragraph>
            <Button
              type="primary"
              style={{ margin: 10 }}
              onClick={this.openStartTestModal}
            >
              Start
            </Button>
            <Modal
              title="Instructions"
              visible={this.state.visible}
              onCancel={this.closeModal}
              onOk={this.closeModal}
              destroyOnClose={true}
            >
              <iframe
                title="Title"
                src={this.props.videoUrl}
                frameborder="10"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture;fullscreen"
              ></iframe>
            </Modal>
          </Typography>
        </Card>
        <Modal
          title="Start Test"
          visible={this.state.startTestModal}
          onCancel={this.closeStartTestModal}
          destroyOnClose={true}
          footer={[
            <Button key="back" onClick={this.closeStartTestModal}>
              Close
            </Button>,
          ]}
        >
          <Card>
            <p>
              Read instructions carefully before starting test.Once test started
              you can not go back to read instruction. If you use browser back
              button test will be terminated.
            </p>
            <Button type="primary" onClick={this.props.startTest}>
              Start Test
            </Button>
          </Card>
        </Modal>
      </div>
    );
  }
}

export default DiagramInstruction;
