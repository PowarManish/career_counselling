import React, { Component } from "react";
import DiagramInstruction from "../DiagramInstruction";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class FM extends Component {
  startTest = () => {
    const data = {
      pageNumber: 14,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/fm");
    }
  }

  render() {
    return (
      <div>
        <DiagramInstruction
          testName="FM.testName"
          instructionPart1="FM.instructionPart1"
          question3="FM.question3"
          image1={require("../../../../images/FM_practiceproblem1.png")}
          image2={require("../../../../images/FM_practiceproblem2.png")}
          instructionPart2="FM.instructionPart2"
          videoUrl="https://www.youtube.com/embed/VQICR4msEO0"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FM);
