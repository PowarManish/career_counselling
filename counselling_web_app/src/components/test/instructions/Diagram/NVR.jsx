import React, { Component } from "react";
import DiagramInstruction from "../DiagramInstruction";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class NVR extends Component {
  startTest = () => {
    const data = {
      pageNumber: 16,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/nvr");
    }
  }

  render() {
    return (
      <div>
        <DiagramInstruction
          testName="NVR.testName"
          instructionPart1="NVR.instructionPart1"
          question1="NVR.question1"
          question2="NVR.question2"
          question3="NVR.question3"
          question4="NVR.question4"
          question5="NVR.question5"
          image1={require("../../../../images/NVR_practiceproblem1.png")}
          image2={require("../../../../images/NVR_practiceproblem2.png")}
          image3={require("../../../../images/NVR_practiceproblem3.png")}
          image4={require("../../../../images/NVR_practiceproblem4.png")}
          image5={require("../../../../images/NVR_practiceproblem5.png")}
          instructionPart2="NVR.instructionPart2"
          videoUrl="https://www.youtube.com/embed/XMzJzrVl-kg"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NVR);
