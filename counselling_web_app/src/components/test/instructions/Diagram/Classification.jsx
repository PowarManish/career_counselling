import React, { Component } from "react";
import DiagramInstruction from "../DiagramInstruction";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class Classification extends Component {
  startTest = () => {
    const data = {
      pageNumber: 31,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/classification");
    }
  }

  render() {
    return (
      <div>
        <DiagramInstruction
          testName="Classification.testName"
          instructionPart1="Classification.instructionPart1"
          question1="Classification.question1"
          question2="Classification.question2"
          question3="Classification.question3"
          question4="Classification.question4"
          question5="Classification.question5"
          question6="Classification.question6"
          image1={require("../../../../images/Classification_practiceproblem1.png")}
          image2={require("../../../../images/Classification_practiceproblem2.png")}
          image3={require("../../../../images/Classification_practiceproblem3.png")}
          image4={require("../../../../images/Classification_practiceproblem4.png")}
          image5={require("../../../../images/Classification_practiceproblem5.png")}
          image6={require("../../../../images/Classification_practiceproblem6.png")}
          instructionPart2="Classification.instructionPart2"
          videoUrl="https://www.youtube.com/embed/S8GlzkOCrDs"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Classification);
