import React, { Component } from "react";
import DiagramInstruction from "../DiagramInstruction";
import {
  savePageNumber,
  clearSavePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class Analogy extends Component {
  startTest = () => {
    const data = {
      pageNumber: 25,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/analogy");
    }
  }

  render() {
    return (
      <div>
        <DiagramInstruction
          testName="Analogy.testName"
          instructionPart1="Analogy.instructionPart1"
          question1="Analogy.question1"
          question2="Analogy.question2"
          question3="Analogy.question3"
          question4="Analogy.question4"
          question5="Analogy.question5"
          question6="Analogy.question6"
          image1={require("../../../../images/Analogy_practiceproblem1.png")}
          image2={require("../../../../images/Analogy_practiceproblem2.png")}
          image3={require("../../../../images/Analogy_practiceproblem3.png")}
          image4={require("../../../../images/Analogy_practiceproblem4.png")}
          image5={require("../../../../images/Analogy_practiceproblem5.png")}
          image6={require("../../../../images/Analogy_practiceproblem6.png")}
          instructionPart2="Analogy.instructionPart2"
          videoUrl="https://www.youtube.com/embed/GTZeGOCEKQ4"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Analogy);
