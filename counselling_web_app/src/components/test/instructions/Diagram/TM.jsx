import React, { Component } from "react";
import DiagramInstruction from "../DiagramInstruction";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class TM extends Component {
  startTest = () => {
    const data = {
      pageNumber: 10,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/tm");
    }
  }

  render() {
    return (
      <div>
        <DiagramInstruction
          testName="TM.testName"
          instructionPart1="TM.instructionPart1"
          question1="TM.question1"
          question2="TM.question2"
          question3="TM.question3"
          image1={require("../../../../images/TM_practiceproblem1.png")}
          image2={require("../../../../images/TM_practiceproblem2.png")}
          image3={require("../../../../images/TM_practiceproblem3.png")}
          instructionPart2="TM.instructionPart2"
          videoUrl="https://www.youtube.com/embed/a8ZEe9LWCcI"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TM);
