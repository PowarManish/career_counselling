import React, { Component } from "react";
import DiagramInstruction from "../DiagramInstruction";
import {
  savePageNumber,
  clearSavePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class TDS extends Component {
  startTest = () => {
    const data = {
      pageNumber: 6,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/tds");
    }
  }
  render() {
    return (
      <div>
        <DiagramInstruction
          testName="TDS.testName"
          instructionPart1="TDS.instructionPart1"
          question1="TDS.question1"
          question2="TDS.question2"
          question3="TDS.question3"
          question4="TDS.question4"
          question5="TDS.question5"
          image1={require("../../../../images/TDS_practiceproblem1.png")}
          image2={require("../../../../images/TDS_practiceproblem2.png")}
          image3={require("../../../../images/TDS_practiceproblem3.png")}
          image4={require("../../../../images/TDS_practiceproblem4.png")}
          image5={require("../../../../images/TDS_practiceproblem5.png")}
          instructionPart2="TDS.instructionPart2"
          videoUrl="https://www.youtube.com/embed/lRWGB0ir-hU"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TDS);
