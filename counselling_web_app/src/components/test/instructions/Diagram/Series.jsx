import React, { Component } from "react";
import DiagramInstruction from "../DiagramInstruction";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class Series extends Component {
  startTest = () => {
    const data = {
      pageNumber: 29,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/series");
    }
  }

  render() {
    return (
      <div>
        <DiagramInstruction
          testName="Series.testName"
          instructionPart1="Series.instructionPart1"
          question1="Series.question1"
          question2="Series.question2"
          question3="Series.question3"
          question4="Series.question4"
          question5="Series.question5"
          question6="Series.question6"
          image1={require("../../../../images/Series_practiceproblem1.png")}
          image2={require("../../../../images/Series_practiceproblem2.png")}
          image3={require("../../../../images/Series_practiceproblem3.png")}
          image4={require("../../../../images/Series_practiceproblem4.png")}
          image5={require("../../../../images/Series_practiceproblem5.png")}
          image6={require("../../../../images/Series_practiceproblem6.png")}
          instructionPart2="Series.instructionPart2"
          videoUrl="https://www.youtube.com/embed/brHIjy84k_w"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Series);
