import React, { Component } from "react";
import DiagramInstruction from "../DiagramInstruction";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class Reflection extends Component {
  startTest = () => {
    const data = {
      pageNumber: 27,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/reflection");
    }
  }

  render() {
    return (
      <div>
        <DiagramInstruction
          testName="Reflection.testName"
          instructionPart1="Reflection.instructionPart1"
          question1="Reflection.question1"
          question2="Reflection.question2"
          question3="Reflection.question3"
          question4="Reflection.question4"
          question5="Reflection.question5"
          question6="Reflection.question6"
          image1={require("../../../../images/Reflection_practiceproblem1.png")}
          image2={require("../../../../images/Reflection_practiceproblem2.png")}
          image3={require("../../../../images/Reflection_practiceproblem3.png")}
          image4={require("../../../../images/Reflection_practiceproblem4.png")}
          image5={require("../../../../images/Reflection_practiceproblem5.png")}
          image6={require("../../../../images/Reflection_practiceproblem6.png")}
          instructionPart2="Reflection.instructionPart2"
          videoUrl="https://www.youtube.com/embed/-g3YoYQ93gs"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Reflection);
