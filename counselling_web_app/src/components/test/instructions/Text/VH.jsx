import React, { Component } from "react";
import Instruction from "../Instruction";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class VH extends Component {
  startTest = () => {
    const data = {
      pageNumber: 21,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/vh");
    }
  }

  render() {
    return (
      <div>
        <Instruction
          testName="VH.testName"
          instructionPart1="VH.instructionPart1"
          questions="VH.sampleQuestions"
          instructionPart2="VH.instructionPart2"
          videoUrl="https://www.youtube.com/embed/L4vPr6xPPFQ"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(VH);
