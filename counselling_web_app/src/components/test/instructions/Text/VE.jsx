import React, { Component } from "react";
import Instruction from "../Instruction";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class VE extends Component {
  startTest = () => {
    const data = {
      pageNumber: 8,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/ve");
    }
  }

  render() {
    return (
      <div>
        <Instruction
          testName="VE.testName"
          instructionPart1="VE.instructionPart1"
          questions="VE.sampleQuestions"
          instructionPart2="VE.instructionPart2"
          videoUrl="https://www.youtube.com/embed/eFMBWlGn1_Q"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(VE);
