import React, { Component } from "react";
import Instruction from "../Instruction";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import * as localStorage from "../../../../utility/localStorage";
import { connect } from "react-redux";

export class NC extends Component {
  startTest = () => {
    const data = {
      pageNumber: 2,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/nc");
    }
  }

  render() {
    return (
      <div>
        <Instruction
          testName="NC.testName"
          instructionPart1="NC.instructionPart1"
          questions="NC.sampleQuestions"
          instructionPart2="NC.instructionPart2"
          videoUrl="https://www.youtube.com/embed/dJDIL2k_d8g"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NC);
