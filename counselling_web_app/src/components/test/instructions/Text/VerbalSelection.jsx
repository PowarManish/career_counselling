import React, { Component } from "react";
import { Button, Card, Form, Select, Typography } from "antd";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";
import * as localStorage from "../../../../utility/localStorage";

const { Title, Paragraph } = Typography;
const { Option } = Select;

export class VerbalSelection extends Component {
  onFinish = (values) => {
    console.log("Finish", values);
    if (values.language === "Marathi") {
      localStorage.setItem("verbal_language", "Marathi");
      const data = {
        pageNumber: 18,
        candidateId: localStorage.getItem("candidate_id"),
      };
      this.props.savePageNumber(data);
    } else if (values.language === "Hindi") {
      localStorage.setItem("verbal_language", "Hindi");
      const data = {
        pageNumber: 20,
        candidateId: localStorage.getItem("candidate_id"),
      };
      this.props.savePageNumber(data);
    }
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      if (localStorage.getItem("verbal_language") === "Marathi") {
        this.props.history.replace("/instruction/vm");
      } else if (localStorage.getItem("verbal_language") === "Hindi") {
        this.props.history.replace("/instruction/vh");
      }
    }
  }

  render() {
    return (
      <div>
        <Card
          style={{
            width: 800,
            margin: 20,
          }}
        >
          <Title level={2} style={{}}>
            Verbal Test
          </Title>
          <Paragraph>
            <b>
              In this test some words are given We have to find pair of words of
              same meaning (synonyms) or of opposite meaning (antonyms) and
              choose the correct answer.You can give this test either in Marathi
              or Hindi.Please select appropriate language.
            </b>
          </Paragraph>
          <Form onFinish={this.onFinish}>
            <Form.Item
              label="Language"
              name="language"
              rules={[{ required: true, message: "Please select language" }]}
            >
              <Select placeholder="Select a language">
                <Option value="Marathi">Marathi</Option>
                <Option value="Hindi">Hindi</Option>
              </Select>
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(VerbalSelection);
