import React, { Component } from "react";
import Instruction from "../Instruction";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class AC extends Component {
  startTest = () => {
    const data = {
      pageNumber: 4,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/ac");
    }
  }

  render() {
    return (
      <div>
        <Instruction
          testName="AC.testName"
          instructionPart1="AC.instructionPart1"
          questions="AC.sampleQuestions"
          instructionPart2="AC.instructionPart2"
          videoUrl="https://www.youtube.com/embed/df1ZB0RUpZE"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AC);
