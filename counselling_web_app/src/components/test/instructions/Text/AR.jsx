import React, { Component } from "react";
import Instruction from "../Instruction";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class AR extends Component {
  startTest = () => {
    const data = {
      pageNumber: 12,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/ar");
    }
  }

  render() {
    return (
      <div>
        <Instruction
          testName="AR.testName"
          instructionPart1="AR.instructionPart1"
          questions="AR.sampleQuestions"
          instructionPart2="AR.instructionPart2"
          videoUrl="https://www.youtube.com/embed/pZuaG3vfUJ8"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AR);
