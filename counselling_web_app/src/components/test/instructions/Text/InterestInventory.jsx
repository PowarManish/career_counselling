import React, { Component } from "react";
import Instruction from "../Instruction";
import {
  clearSavePageNumber,
  savePageNumber,
} from "../../../../actions/candidateActions";
import { connect } from "react-redux";

export class InterestInventory extends Component {
  startTest = () => {
    const data = {
      pageNumber: 33,
      candidateId: localStorage.getItem("candidate_id"),
    };
    this.props.savePageNumber(data);
  };

  componentDidUpdate() {
    if (this.props.isPageNumberSaved) {
      this.props.clearSavePageNumber();
      this.props.history.replace("/test/interest");
    }
  }

  render() {
    return (
      <div>
        <Instruction
          testName="Interest.testName"
          instructionPart1="Interest.instructionPart1"
          questions="Interest.sampleQuestions"
          instructionPart2="Interest.instructionPart2"
          videoUrl="https://www.youtube.com/embed/Xh1GRF8gbyo"
          startTest={this.startTest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isPageNumberSaved: state.candidateReducer.isPageNumberSaved,
});
const mapDispatchToProps = (dispatch) => {
  return {
    savePageNumber: (values) => dispatch(savePageNumber(values)),
    clearSavePageNumber: () => dispatch(clearSavePageNumber()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InterestInventory);
