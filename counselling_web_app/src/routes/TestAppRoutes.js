import TestAppLayout from "../components/TestAppLayout";
// import Home from "../Home";
import Information from "../components/test/candidate/Information";
import Payment from "../components/test/candidate/Payment";
import NC from "../components/test/instructions/Text/NC";
import AC from "../components/test/instructions/Text/AC";
import TDS from "../components/test/instructions/Diagram/TDS";
import VE from "../components/test/instructions/Text/VE";
import VM from "../components/test/instructions/Text/VM";
import VH from "../components/test/instructions/Text/VH";
import TM from "../components/test/instructions/Diagram/TM";
import FM from "../components/test/instructions/Diagram/FM";
import AR from "../components/test/instructions/Text/AR";
import NVR from "../components/test/instructions/Diagram/NVR";
import Analogy from "../components/test/instructions/Diagram/Analogy";
import Reflection from "../components/test/instructions/Diagram/Reflection";
import Series from "../components/test/instructions/Diagram/Series";
import Classification from "../components/test/instructions/Diagram/Classification";
import Adjustment from "../components/test/instructions/Text/Adjustment";
import InterestInventory from "../components/test/instructions/Text/InterestInventory";
import QuestionAR from "../components/test/questions/AR";
import QuestionTDS from "../components/test/questions/TDS";
import QuestionNC from "../components/test/questions/NC";
import QuestionAC from "../components/test/questions/AC";
import QuestionVE from "../components/test/questions/VE";
import QuestionNVR from "../components/test/questions/NVR";
import QuestionFM from "../components/test/questions/FM";
import QuestionTM from "../components/test/questions/TM";
import QuestionAnalogy from "../components/test/questions/Analogy";
import QuestionReflection from "../components/test/questions/Reflection";
import QuestionSeries from "../components/test/questions/Series";
import QuestionClassification from "../components/test/questions/Classification";
import QuestionAdjustment from "../components/test/questions/Adjustment";
import QuestionInterest from "../components/test/questions/Interest";
import QuestionVM from "../components/test/questions/VM";
import QuestionVH from "../components/test/questions/VH";
import VerbalSelection from "../components/test/instructions/Text/VerbalSelection";
import Result from "../components/result";
import ResumeTest from "../components/test/ResumeTest";
const routes = [
  {
    path: "/",
    exact: true,
    component: TestAppLayout,
    routes: [
      // {
      //   path: "/",
      //   exact: true,
      //   component: Home,
      // },
      {
        path: "/candidateInfo",
        exact: true,
        component: Information,
      },
      {
        path: "/resume",
        exact: true,
        component: ResumeTest,
      },
      {
        path: "/payment",
        exact: true,
        component: Payment,
      },
      {
        path: "/instruction/nc",
        exact: true,
        component: NC,
      },
      {
        path: "/instruction/ac",
        exact: true,
        component: AC,
      },
      {
        path: "/instruction/tds",
        exact: true,
        component: TDS,
      },
      {
        path: "/instruction/ve",
        exact: true,
        component: VE,
      },
      {
        path: "/instruction/vm",
        exact: true,
        component: VM,
      },
      {
        path: "/instruction/vh",
        exact: true,
        component: VH,
      },
      {
        path: "/instruction/tm",
        exact: true,
        component: TM,
      },
      {
        path: "/instruction/fm",
        exact: true,
        component: FM,
      },
      {
        path: "/instruction/ar",
        exact: true,
        component: AR,
      },
      {
        path: "/instruction/nvr",
        exact: true,
        component: NVR,
      },
      {
        path: "/instruction/analogy",
        exact: true,
        component: Analogy,
      },
      {
        path: "/instruction/Reflection",
        exact: true,
        component: Reflection,
      },
      {
        path: "/instruction/Series",
        exact: true,
        component: Series,
      },
      {
        path: "/instruction/Classification",
        exact: true,
        component: Classification,
      },
      {
        path: "/instruction/interest",
        exact: true,
        component: InterestInventory,
      },
      {
        path: "/instruction/adjustment",
        exact: true,
        component: Adjustment,
      },
      {
        path: "/test/ar",
        exact: true,
        component: QuestionAR,
      },
      {
        path: "/test/tds",
        exact: true,
        component: QuestionTDS,
      },
      {
        path: "/test/nc",
        exact: true,
        component: QuestionNC,
      },
      {
        path: "/test/ac",
        exact: true,
        component: QuestionAC,
      },
      {
        path: "/test/ve",
        exact: true,
        component: QuestionVE,
      },
      {
        path: "/test/nvr",
        exact: true,
        component: QuestionNVR,
      },
      {
        path: "/test/fm",
        exact: true,
        component: QuestionFM,
      },
      {
        path: "/test/tm",
        exact: true,
        component: QuestionTM,
      },
      {
        path: "/test/analogy",
        exact: true,
        component: QuestionAnalogy,
      },
      {
        path: "/test/reflection",
        exact: true,
        component: QuestionReflection,
      },
      {
        path: "/test/series",
        exact: true,
        component: QuestionSeries,
      },
      {
        path: "/test/classification",
        exact: true,
        component: QuestionClassification,
      },

      {
        path: "/test/adjustment",
        exact: true,
        component: QuestionAdjustment,
      },
      {
        path: "/test/interest",
        exact: true,
        component: QuestionInterest,
      },
      {
        path: "/test/vm",
        exact: true,
        component: QuestionVM,
      },
      {
        path: "/test/vh",
        exact: true,
        component: QuestionVH,
      },
      {
        path: "/instruction/verbal",
        exact: true,
        component: VerbalSelection,
      },
      {
        path: "/result",
        exact: true,
        component: Result,
      },
    ],
  },
];

export default routes;
