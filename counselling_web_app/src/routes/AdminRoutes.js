import Login from "../components/admin/Login";
import Dashboard from "../components/admin/Dashboard";
import Candidate from "../components/admin/CandidateInfo";
import CandidateDetails from "../components/admin/CandidateDetails";
import Result from "../components/admin/Result";
import AdminLayout from "../components/AdminLayout";

const routes = [
  {
    path: "/admin",
    exact: true,
    component: AdminLayout,
    routes: [
      {
        path: "/admin/login",
        exact: true,
        component: Login,
      },
      {
        path: "/admin/dashboard",
        exact: true,
        component: Dashboard,
      },
      {
        path: "/admin/viewCandidate",
        exact: true,
        component: Candidate,
      },
      {
        path: "/admin/candidateDetails",
        exact: true,
        component: CandidateDetails,
      },
      {
        path: "/admin/result",
        exact: true,
        component: Result,
      },
    ],
    // routes: [
    //   {
    //     path: "/admin/dashboard",
    //     exact: true,
    //     component: Dashboard,
    //   },
    // ],
  },
  // {
  //   path: "/admin/dashboard",
  //   exact: true,
  //   component: Dashboard,
  //   // routes: [
  //   //   {
  //   //     path: "/admin/dashboard",
  //   //     exact: true,
  //   //     component: Dashboard,
  //   //   },
  //   // ],
  // },
  // {
  //   path: "/admin/viewCandidate",
  //   exact: true,
  //   component: Candidate,
  // },
  // {
  //   path: "/admin/candidateDetails",
  //   exact: true,
  //   component: CandidateDetails,
  // },
  // {
  //   path: "/admin/result",
  //   exact: true,
  //   component: Result,
  // },
  // {
  //     component: NotFound
  // }
];

export default routes;
