import { combineReducers } from "redux";
import adminReducer from "./adminReducer";
import candidateReducer from "./candidateReducer";
import testReducer from "./testReducer";

export default combineReducers({
  adminReducer,
  candidateReducer,
  testReducer,
});
