import {
  AUTH_LOGIN,
  CONCLUSION,
  CONCLUSION_MAIL,
} from "../actions/actionTypes";
const defaultState = {
  isLoggedIn: null,
  isConclusionPosted: false,
  isConclusionMailSent: false,
};

export default function adminReducer(state = defaultState, action) {
  switch (action.type) {
    case AUTH_LOGIN:
      return {
        ...state,
        isLoggedIn: action.payload,
      };
    case CONCLUSION:
      return {
        ...state,
        isConclusionPosted: action.payload.isDataUpdated,
      };
    case CONCLUSION_MAIL:
      return {
        ...state,
        isConclusionMailSent: action.payload.isMailSent,
      };

    default:
      return state;
  }
}
