import {
  CANDIDATE_INFO,
  CANDIDATE_TABLE_DATA,
  RESUME_TEST,
  CLEAR_RESUME_FLAG,
  SEND_MAIL,
  SAVE_PAGE_NUMBER,
} from "../actions/actionTypes";
import { createBrowserHistory } from "history";

export const history = createBrowserHistory();
const defaultState = {
  candidateId: 0,
  candidate: [],
  isResumeTest: null,
  resumeTestData: [],
  isMailSent: false,
  resumeTestPage: 0,
  isPageNumberSaved: false,
};

export default function adminReducer(state = defaultState, action, props) {
  console.log(action);
  switch (action.type) {
    case CANDIDATE_INFO:
      return {
        ...state,
        candidateId: action.payload.candidateId,
      };
    case CANDIDATE_TABLE_DATA:
      return {
        ...state,
        candidate: action.payload,
      };
    case RESUME_TEST:
      return {
        ...state,
        isResumeTest: action.payload.isResumeTest,
        resumeTestData: action.payload.resumeData,
        resumeTestPage: action.payload.pageNumber,
        candidateId: action.payload.candidateId,
      };
    case CLEAR_RESUME_FLAG:
      return {
        ...state,
        isResumeTest: action.payload,
      };
    case SEND_MAIL:
      return {
        ...state,
        isMailSent: action.payload.isMailSent,
      };
    case SAVE_PAGE_NUMBER:
      return {
        ...state,
        isPageNumberSaved: action.payload.isDataChanged,
      };
    default:
      return state;
  }
}
