import { TEST, RESULT, ADJUSTMENT_ANS, SUBMIT } from "../actions/actionTypes";
const defaultState = {
  //   answerSaved: false,
  test: [],
  result: [],
  adjustmentAnswers: [],
  isTestSubmitted: false,
};

export default function testReducer(state = defaultState, action) {
  switch (action.type) {
    // case SAVE_ANSWER:
    //   return {
    //     ...state,
    //     answerSaved: action.payload,
    //   };
    case TEST:
      return {
        ...state,
        test: action.payload,
      };
    case RESULT:
      return {
        ...state,
        result: action.payload,
      };
    case ADJUSTMENT_ANS:
      return {
        ...state,
        adjustmentAnswers: action.payload,
      };
    case SUBMIT:
      return {
        ...state,
        isTestSubmitted: action.payload.isDataChanged,
      };
    default:
      return state;
  }
}
