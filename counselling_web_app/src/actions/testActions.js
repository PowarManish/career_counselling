import {
  SAVE_ANSWER,
  TEST,
  SUBMIT,
  RESULT,
  SEND_MAIL,
  ADJUSTMENT_ANS,
} from "./actionTypes";
const actionHelper = require("./actionHelper");

export const saveAnswer = (data) => {
  return actionHelper.postData(data, SAVE_ANSWER, "test/save_answer");
};

export const updateAnswer = (data) => {
  return actionHelper.updateData(data, SAVE_ANSWER, "test/save_answer");
};

export const fetchTest = () => {
  return actionHelper.getData(TEST, "test");
};

export const submitTest = (data) => {
  return actionHelper.postData(data, SUBMIT, "test/submit");
};

export const getResult = (params) => {
  return actionHelper.getData(RESULT, "test/result", params);
};

export const sendMail = (data) => {
  return actionHelper.postData(data, SEND_MAIL, "candidate/mailResult");
};

export const clearMailSent = () => {
  return actionHelper.clearFlag(SEND_MAIL, { isMailSent: false });
};

export const getAdjustmentAnswers = (params) => {
  return actionHelper.getData(ADJUSTMENT_ANS, "test/adjustmentAns", params);
};

export const clearSubmitTest = () => {
  return actionHelper.clearFlag(SUBMIT, { isDataChanged: false });
};
