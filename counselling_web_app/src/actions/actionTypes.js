//Admin
export const AUTH_LOGIN = "AUTH_LOGIN";
export const CONCLUSION = "CONCLUSION";
export const CONCLUSION_MAIL = "CONCLUSION_MAIL";

//Candidate

export const CANDIDATE_INFO = "CANDIDATE_INFO ";
export const CANDIDATE_TABLE_DATA = "CANDIDATE_TABLE_DATA ";
export const RESUME_TEST = "RESUME_TEST ";
export const CLEAR_RESUME_FLAG = "CLEAR_RESUME_FLAG";
export const SAVE_PAGE_NUMBER = "SAVE_PAGE_NUMBER";

//Test

export const SAVE_ANSWER = "SAVE_ANSWER";
export const TEST = "TEST";
export const SUBMIT = "SUBMIT";
export const RESULT = "RESULT";
export const SEND_MAIL = "SEND_MAIL";
export const ADJUSTMENT_ANS = "ADJUSTMENT_ANS";
