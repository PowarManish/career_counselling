import axios from "axios";
export const getData = (type, path, params) => {
  var url = "";
  url = "https://apicareerdishaforyou.tk/api/" + path;
  return (dispatch) => {
    axios
      .get(url, { params: params })
      .then((response) => {
        dispatch({
          type: type,
          payload: response.data.data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const clearFlag = (type, value) => {
  return (dispatch) => {
    dispatch({
      type: type,
      payload: value,
    });
  };
};

export const postData = (data, type, path) => {
  var url = "";
  url = "https://apicareerdishaforyou.tk/api/" + path;
  return (dispatch) => {
    axios({
      method: "post",
      url: url,
      data: JSON.stringify({ data }),
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => {
        //handle success
        console.log(response);
        // if (response.data.isDataChanged) {
        //   message.success("Added Successfully", 1.5);
        // }
        dispatch({
          type: type,
          payload: response.data,
        });
      })
      .catch(function (error) {
        //handle error
        console.log(error);
      });
  };
};

export const updateData = (data, type, path) => {
  var url = "";
  url = "https://apicareerdishaforyou.tk/api/" + path;
  return (dispatch) => {
    axios({
      method: "put",
      url: url,
      data: JSON.stringify({ data: data }),
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => {
        if (response.data.isDataChanged) {
          // message.success("Updated Successfully", 1.5);
        }
        //handle success
        dispatch({
          type: type,
          payload: response.data.isDataChanged,
        });
      })
      .catch(function (error) {
        //handle error
        console.log(error);
      });
  };
};
