import {
  CANDIDATE_INFO,
  CANDIDATE_TABLE_DATA,
  RESUME_TEST,
  CLEAR_RESUME_FLAG,
  SAVE_PAGE_NUMBER,
} from "./actionTypes";
const actionHelper = require("./actionHelper");

export const postCandidateInfo = (data) => {
  return actionHelper.postData(data, CANDIDATE_INFO, "candidate/info");
};

export const getCandidateInfo = () => {
  return actionHelper.getData(CANDIDATE_TABLE_DATA, "candidate/table_data");
};

export const resumeTest = (params) => {
  return actionHelper.getData(RESUME_TEST, "candidate/resume_test", params);
};

export const clearWrongResumeCredential = () => {
  return actionHelper.clearFlag(CLEAR_RESUME_FLAG, null);
};

export const savePageNumber = (data) => {
  return actionHelper.postData(
    data,
    SAVE_PAGE_NUMBER,
    "candidate/savePageNumber"
  );
};

export const clearSavePageNumber = () => {
  return actionHelper.clearFlag(SAVE_PAGE_NUMBER, { isDataChanged: false });
};
