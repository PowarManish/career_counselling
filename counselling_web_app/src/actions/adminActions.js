import { AUTH_LOGIN, CONCLUSION, CONCLUSION_MAIL } from "./actionTypes";
const actionHelper = require("./actionHelper");

export const authenticateLogin = (params) => {
  return actionHelper.getData(AUTH_LOGIN, "admin/login", params);
};

export const logout = () => {
  return actionHelper.clearFlag(AUTH_LOGIN, null);
};

export const postCounsellingConclusion = (data) => {
  return actionHelper.postData(data, CONCLUSION, "admin/counselling");
};

export const clearWrongAdminCredential = () => {
  return actionHelper.clearFlag(AUTH_LOGIN, null);
};

export const mailCounsellingConclusion = (data) => {
  return actionHelper.postData(
    data,
    CONCLUSION_MAIL,
    "admin/mailConsellingResult"
  );
};

export const clearConclusionPosted = () => {
  return actionHelper.clearFlag(CONCLUSION, { isDataUpdated: false });
};

export const clearConclusionMailSent = () => {
  return actionHelper.clearFlag(CONCLUSION_MAIL, { isMailSent: false });
};
