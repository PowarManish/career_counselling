import { Radio } from "antd";
import React from "react";
import { Switch, Route } from "react-router-dom";

export function renderRoutes(routes, extraProps = {}, switchProps = {}) {
  return routes ? (
    <Switch {...switchProps}>
      {routes.map((route, i) => (
        <Route
          key={route.key || i}
          path={route.path}
          exact={route.exact}
          strict={route.strict}
          render={(props) =>
            route.render ? (
              route.render({ ...props, ...extraProps, route: route })
            ) : (
              <route.component {...props} {...extraProps} route={route} />
            )
          }
        />
      ))}
    </Switch>
  ) : null;
}

export function getOptionsByQustion(testName, qindex, messages) {
  const optionValues = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
  const optionKeys = Object.keys(messages).filter((option) =>
    option.includes("questions." + testName + "." + qindex + ".options")
  );
  return optionKeys.map((option, index) => (
    <Radio key={index} value={optionValues[index]}>
      {messages[`${option}`]}
    </Radio>
  ));
}

export function getTestIdFromTestName(test, testName) {
  for (let i = 0; i < test.length; i++) {
    if (test[i].name === testName) {
      return test[i].id;
    }
  }
}

export function getCandidateNameFromCandidateID(candidate, candidateId) {
  for (let i = 0; i < candidate.length; i++) {
    if (candidate[i].id === candidateId) {
      return candidate[i].name;
    }
  }
}

export function getConclusionFromCandidateID(candidate, candidateId) {
  for (let i = 0; i < candidate.length; i++) {
    if (candidate[i].id === candidateId) {
      return candidate[i].Conclusion;
    }
  }
}
