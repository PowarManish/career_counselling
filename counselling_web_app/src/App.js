import React from "react";
import { withRouter } from "react-router-dom";
import TestAppRoutes from "./routes/TestAppRoutes";
import AdminRoutes from "./routes/AdminRoutes";
import RouteWithSubRoutes from "./components/common/RouteWithSubRoutes";

const App = (props) => {
  return (
    <>
      {AdminRoutes.map((route) => (
        <RouteWithSubRoutes key={route.path} {...route} />
      ))}
      {TestAppRoutes.map((route) => (
        <RouteWithSubRoutes key={route.path} {...route} />
      ))}
    </>
  );
};

export default withRouter(App);
